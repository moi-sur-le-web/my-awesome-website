#!/bin/bash

DATABASE_NAME="myawesomedatabase"
SCHEMA_NAME="myawesomeschema"
USER_NAME="myawesomeuser"
USER_PASSWORD="{{ DATABASE_PASSWORD }}"

# Création de la base de données
psql -U postgres -c "CREATE DATABASE $DATABASE_NAME;" || { echo "Erreur lors de la création de la base de données."; exit 1; }

# Création de l'utilisateur avec le mot de passe spécifié
psql -U postgres -c "CREATE USER $USER_NAME WITH ENCRYPTED PASSWORD '$USER_PASSWORD';" || { echo "Erreur lors de la création de l'utilisateur."; exit 1; }

# Définition du propriétaire de la base de données
psql -U postgres -c "ALTER DATABASE $DATABASE_NAME OWNER TO $USER_NAME;" || { echo "Erreur lors de la définition du propriétaire de la base de données."; exit 1; }

# Création d'un schéma dans la base de données
psql -U $USER_NAME -d $DATABASE_NAME -c "CREATE SCHEMA $SCHEMA_NAME;" || { echo "Erreur lors de la création du schéma."; exit 1; }

# Définition du schéma par défaut pour l'utilisateur
psql -U postgres -c "ALTER USER $USER_NAME SET SEARCH_PATH TO $SCHEMA_NAME;" || { echo "Erreur lors de la définition du schéma par défaut."; exit 1; }

echo "Le script s'est exécuté avec succès."