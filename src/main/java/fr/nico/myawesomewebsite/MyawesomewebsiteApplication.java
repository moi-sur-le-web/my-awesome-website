package fr.nico.myawesomewebsite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyawesomewebsiteApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyawesomewebsiteApplication.class, args);
    }
}