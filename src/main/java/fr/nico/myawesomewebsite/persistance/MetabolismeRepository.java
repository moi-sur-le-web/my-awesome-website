package fr.nico.myawesomewebsite.persistance;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.nico.myawesomewebsite.domaine.entities.Metabolisme;

/**
 * The Interface MetabolismeRepository.
 */
@Repository
public interface MetabolismeRepository extends JpaRepository<Metabolisme, Long>{

    /**
     * Gets the metabolismes by user id.
     *
     * @param id the id
     * @return the metabolismes by user id
     */
    @Query("select met from Utilisateur uti "
            + "left join uti.metabolismes met "
            + "where uti.id = :id "
            + "order by met.valeur asc")
    Optional<List<Metabolisme>> getMetabolismesByUserId(@Param("id") Integer id);

}
