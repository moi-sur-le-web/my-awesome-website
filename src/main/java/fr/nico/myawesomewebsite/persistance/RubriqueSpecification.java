package fr.nico.myawesomewebsite.persistance;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import fr.nico.myawesomewebsite.domaine.entities.Fil;
import fr.nico.myawesomewebsite.domaine.referentiel.ProfilEnum;

/**
 * The Class RubriqueSpecification.
 */
public class RubriqueSpecification implements Specification<Fil> {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3558636054724358614L;

    /** The libelle. */
    private String libelle;
    
    /** The profil. */
    private ProfilEnum profil;

    /**
     * Instantiates a new rubrique specification.
     *
     * @param libelle the libelle
     */
    public RubriqueSpecification(String libelle) {
        this.libelle = libelle;
    }
    
    /**
     * Instantiates a new rubrique specification.
     *
     * @param libelle the libelle
     * @param profil the profil
     */
    public RubriqueSpecification(String libelle, ProfilEnum profil) {
        this.libelle = libelle;
        this.profil = profil;
    }

    /**
     * To predicate.
     *
     * @param root the root
     * @param query the query
     * @param criteriaBuilder the criteria builder
     * @return the predicate
     */
    @Override
    public Predicate toPredicate(Root<Fil> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        Predicate whereTagEquals = criteriaBuilder.equal(root.join("tags").get("libelle"), this.libelle);
        if (this.profil != null) {
            Predicate whereProfilEquals = criteriaBuilder.equal(root.join("auteur").get("profil"), this.profil);
            return criteriaBuilder.and(whereTagEquals, whereProfilEquals);
        }
        return whereTagEquals;
    }
}
