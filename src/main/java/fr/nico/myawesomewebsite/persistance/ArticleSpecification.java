package fr.nico.myawesomewebsite.persistance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.Authentication;
import org.springframework.util.StringUtils;

import fr.nico.myawesomewebsite.domaine.entities.Fil;
import fr.nico.myawesomewebsite.domaine.entities.Utilisateur;
import fr.nico.myawesomewebsite.webapp.dto.PageRequestDto;

/**
 * The Class ArticleSpecification.
 */
public class ArticleSpecification implements Specification<Fil>{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7228998401521954137L;
    
    /** The Constant COLON_SEPARATOR. */
    private static final String COLON_SEPARATOR = ":";
    
    /** The Constant DOT_DELIMITER. */
    private static final String DOT_DELIMITER = ".";
    
    /** The Constant NOT_WANTED_EMAIL_SYSTEM_DUMMY_FR. */
    private static final String NOT_WANTED_EMAIL_SYSTEM_DUMMY_FR = "system@dummy.fr";
    
    /** The page request dto. */
    private transient PageRequestDto pageRequestDto;
    
    /** The fields list. */
    private List<String> fieldsList;
    
    /** The tag. */
    private String tag;
    
    private Authentication authentication;

    /**
     * Instantiates a new article specification.
     *
     * @param pageResquestDto the page resquest dto
     * @param fieldsList the fields list
     */
    public ArticleSpecification(PageRequestDto pageResquestDto, List<String> fieldsList) {
        this.pageRequestDto = pageResquestDto;
        this.fieldsList = fieldsList;
    }
    
    /**
     * Instantiates a new article specification.
     *
     * @param tag the tag
     * @param pageResquestDto the page resquest dto
     * @param fieldsList the fields list
     */
    public ArticleSpecification(Authentication auth, String tag, PageRequestDto pageResquestDto, List<String> fieldsList) {
        this.authentication = auth;
        this.tag = tag;
        this.pageRequestDto = pageResquestDto;
        this.fieldsList = fieldsList;
    }

    /**
     * To predicate.
     *
     * @param root the root
     * @param query the query
     * @param criteriaBuilder the criteria builder
     * @return the predicate
     */
    @Override
    public Predicate toPredicate(Root<Fil> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        
        Predicate predicate;
        if (this.tag == null || this.tag.isBlank() || this.tag.isEmpty() ) {
            predicate = this.pagePrincipalePredicate(root, criteriaBuilder);            
        } else {
            predicate = this.rubriquePredicate(root, criteriaBuilder);
        }
        return predicate;
    }

    /**
     * Page principale predicate.
     *
     * @param root the root
     * @param query the query
     * @param criteriaBuilder the criteria builder
     * @return the predicate
     */
    private Predicate pagePrincipalePredicate(Root<Fil> root, CriteriaBuilder criteriaBuilder) {
        
        Join<Fil, Utilisateur> auteurJoin = root.join("auteur", JoinType.INNER);
        return criteriaBuilder.notEqual(auteurJoin.get("mail"), NOT_WANTED_EMAIL_SYSTEM_DUMMY_FR);
    }
    
    /**
     * Rubrique predicate.
     *
     * @param root the root
     * @param query the query
     * @param criteriaBuilder the criteria builder
     * @return the predicate
     */
    private Predicate rubriquePredicate(Root<Fil> root, CriteriaBuilder criteriaBuilder) {
        if (this.tag.equals("en-cours") && this.authentication != null) {
            Join<Fil, Utilisateur> auteurJoin = root.join("auteur", JoinType.INNER);
            Predicate auteur = criteriaBuilder.equal(auteurJoin.get("mail"), this.authentication.getName());
            return criteriaBuilder.and(auteur, criteriaBuilder.equal(root.join("tags").get("libelle"), this.tag));
        }
        return criteriaBuilder.equal(root.join("tags").get("libelle"), this.tag);
    }

    /**
     * Gets the sort orders.
     *
     * @param root the root
     * @param criteriaBuilder the criteria builder
     * @return the sort orders
     */
    private List<Order> getSortOrders(Root<Fil> root, CriteriaBuilder criteriaBuilder) {
        List<Order> sortOrders = this.validateSortAndGetSortOrders(this.isValidSortColsAndDirections(), root, criteriaBuilder); 
        if (sortOrders.isEmpty()) {
            sortOrders = new ArrayList<>();
            sortOrders.add(criteriaBuilder.desc(root.get("dateCreation"))); 
        }
        return sortOrders;
    }
    
    /**
     * Checks if is valid sort cols and directions.
     *
     * @return true, if is valid sort cols and directions
     */
    private boolean isValidSortColsAndDirections() {
        
        List<String> sortColsAndDirections = this.pageRequestDto.getSortColsAndDirections();
        
        if (sortColsAndDirections == null || sortColsAndDirections.isEmpty()) {
            return false;
        }
        int index = 0;        
        while (index < sortColsAndDirections.size()) {            
            String[] sortColAndDirection =  StringUtils.split(sortColsAndDirections.get(index++), COLON_SEPARATOR) ;
            if (sortColAndDirection == null) {
                return false;
            }            
            if (! validateSortDirection(sortColAndDirection[1])) {
                return false;
            }            
            if (! this.fieldsList.contains(sortColAndDirection[0])) {
                return false;
            }               
        }
        return true;
    }

    /**
     * Validate sort and get sort orders.
     *
     * @param isSortOrdersValid the is sort orders valid
     * @param root the root
     * @param criteriaBuilder the criteria builder
     * @return the list
     */
    private List<Order> validateSortAndGetSortOrders(boolean isSortOrdersValid, Root<Fil> root, CriteriaBuilder criteriaBuilder) {
        
        if (!isSortOrdersValid) {
            return Collections.emptyList();
        }        
        List<Order> orders = new ArrayList<>();
        int index = 0;        
        while (index < this.pageRequestDto.getSortColsAndDirections().size()) {
            
            String[] sortColAndDirection =  StringUtils.split(this.pageRequestDto.getSortColsAndDirections().get(index++), COLON_SEPARATOR) ;
            if (sortColAndDirection == null) {
                return Collections.emptyList();
            }
            if ("asc".equalsIgnoreCase(sortColAndDirection[1])) {
                orders.add(criteriaBuilder.asc(this.recursiveBuildSortOrdersAndDirections(root, null, sortColAndDirection[0])));                    
            } else {
                orders.add(criteriaBuilder.desc(this.recursiveBuildSortOrdersAndDirections(root, null, sortColAndDirection[0])));                                        
            }                                  
        }        
        return (orders.isEmpty() ? Collections.emptyList() : orders);
    }

    /**
     * Validate sort direction.
     *
     * @param dir the dir
     * @return true, if successful
     */
    private boolean validateSortDirection(String dir) {
        return "asc".equalsIgnoreCase(dir) || "desc".equalsIgnoreCase(dir);
    }
    
    /**
     * Recursive build sort orders and directions.
     *
     * @param root the root
     * @param path the path
     * @param cols the cols
     * @return the path
     */
    private Path<String> recursiveBuildSortOrdersAndDirections(Root<Fil> root, Path<String> path, String cols){
        String[] fields = StringUtils.split(cols, DOT_DELIMITER);
        if (fields == null){
            if (path == null){
                return root.get(cols);
            } else {
                return path.get(cols);
            }
        }
        if (path == null){
            path = root.get(fields[0]);
        } else {
            path = path.get(fields[0]);
        }
        return recursiveBuildSortOrdersAndDirections(root, path, fields[1]);
    }
}
