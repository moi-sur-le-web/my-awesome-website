package fr.nico.myawesomewebsite.persistance;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.nico.myawesomewebsite.domaine.entities.Utilisateur;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, Integer> {

    /**
     * Find by mail.
     *
     * @param mail the mail
     * @return the optional
     */
    Optional<Utilisateur> findByMail(String mail);

    /**
     * Find by mail fetch all (gabarit, métabolisme, rythme entrainement et alimentaire).
     *
     * @param mail the mail
     * @return the optional
     */
    @Query("select uti from Utilisateur uti "
            + "left join fetch uti.gabarit "
            + "left join fetch uti.metabolismes "
            + "left join fetch uti.rythme "
            + "where uti.mail = :mail ")
    Optional<Utilisateur> findByMailFetchAll(@Param("mail") String mail);

    /**
     * Exists by mail.
     *
     * @param mail the mail
     * @return true, if successful
     */
    boolean existsByMail(String mail);
}