package fr.nico.myawesomewebsite.persistance;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.nico.myawesomewebsite.domaine.entities.Ingredient;

/**
 * The Interface IngredientRepository.
 */
@Repository
public interface IngredientRepository extends JpaRepository<Ingredient, Long>{

	/**
	 * Exists.
	 *
	 * @param codeBarre the code barre
	 * @return true, if successful
	 */
	boolean existsByCodeBarre(String codeBarre);

}
