package fr.nico.myawesomewebsite.persistance;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import fr.nico.myawesomewebsite.domaine.entities.Fil;
import fr.nico.myawesomewebsite.domaine.entities.Utilisateur;

/**
 * The Interface FilRepository.
 */
public interface FilRepository extends JpaRepository<Fil, Long>, JpaSpecificationExecutor<Fil> {

    /**
     * Find by id and auteur.
     *
     * @param id the id
     * @param auteur the auteur
     * @return the optional
     */
    Optional<Fil> findByIdAndAuteur(Long id, Utilisateur auteur);

    /**
     * Find fortune of day.
     *
     * @param today the today
     * @return the optional
     */
    @Query("select f from Fil f "
            + "join f.tags t "
            + "where t.libelle = 'fortune' "
            + "and f.dateCreation >= :today")
    Optional<Fil> findFortuneOfDay(LocalDateTime today);
}