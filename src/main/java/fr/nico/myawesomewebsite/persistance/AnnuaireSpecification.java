package fr.nico.myawesomewebsite.persistance;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import fr.nico.myawesomewebsite.domaine.entities.Fil;

public class AnnuaireSpecification implements Specification<Fil> {

    private static final long serialVersionUID = 851090680878487485L;

    @Override
    public Predicate toPredicate(Root<Fil> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

        Predicate tagPredicate = criteriaBuilder.equal(root.join("tags").get("libelle"), "annuaire");
        Predicate lienPredicate = criteriaBuilder.notEqual(root.get("lien"), "");

        return criteriaBuilder.and(tagPredicate, lienPredicate);
    }

}
