package fr.nico.myawesomewebsite.webapp.dto;

import java.io.Serializable;
import java.util.List;

/**
 * The Class SortedPageDto.
 */
public class SortedPageDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -8365359568061626036L;

    /** The page index. */
    private int pageIndex;

    /** The page size. */
    private int pageSize;

    /** The sort cols and directions. */
    private List<String> sortColsAndDirections;

    /** The entity. */
    private Class<?> className;

    /**
     * Gets the page index.
     *
     * @return the page index
     */
    public int getPageIndex() {
        return this.pageIndex;
    }

    /**
     * Sets the page index.
     *
     * @param pageIndex the new page index
     */
    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    /**
     * Gets the page size.
     *
     * @return the page size
     */
    public int getPageSize() {
        return this.pageSize;
    }

    /**
     * Sets the page size.
     *
     * @param pageSize the new page size
     */
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * Gets the sort cols and directions.
     *
     * @return the sort cols and directions
     */
    public List<String> getSortColsAndDirections() {
        return this.sortColsAndDirections;
    }

    /**
     * Sets the sort cols and directions.
     *
     * @param sortColsAndDirections the new sort cols and directions
     */
    public void setSortColsAndDirections(List<String> sortColsAndDirections) {
        this.sortColsAndDirections = sortColsAndDirections;
    }

    /**
     * Gets the class name.
     *
     * @return the class name
     */
    public Class<?> getClassName() {
        return this.className;
    }

    /**
     * Sets the class name.
     *
     * @param className the new class name
     */
    public void setClassName(Class<?> className) {
        this.className = className;
    }

    /**
     * Gets the serialversionuid.
     *
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
