package fr.nico.myawesomewebsite.webapp.dto.fil;

public class OrigineDto {

    private long id;

    private String ecranOrigine;

    private Integer number;

    private Integer size;

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEcranOrigine() {
        return this.ecranOrigine;
    }

    public void setEcranOrigine(String ecranOrigine) {
        this.ecranOrigine = ecranOrigine;
    }

    public Integer getNumber() {
        return this.number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getSize() {
        return this.size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
