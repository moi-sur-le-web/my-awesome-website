package fr.nico.myawesomewebsite.webapp.dto.openfoodfacts;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Class OpenFoodFactsResponseDto.
 */
public class OpenFoodFactsResponseDto implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 755958185940224093L;
	
	/** The product dto. */
	@JsonProperty(value = "product")
	private ProductDto productDto;

	/**
	 * Gets the product dto.
	 *
	 * @return the product dto
	 */
	public ProductDto getProductDto() {
		return productDto;
	}

	/**
	 * Sets the product dto.
	 *
	 * @param productDto the new product dto
	 */
	public void setProductDto(ProductDto productDto) {
		this.productDto = productDto;
	}
}
