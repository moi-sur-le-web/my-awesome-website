package fr.nico.myawesomewebsite.webapp.dto;

import java.util.ArrayList;
import java.util.List;

public class PageRequestDto {

    private Integer number;

    private Integer size;

    private List<String> sortColsAndDirections = new ArrayList<>();

    public Integer getNumber() {
        return this.number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getSize() {
        return this.size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public List<String> getSortColsAndDirections() {
        return this.sortColsAndDirections;
    }

    public void setSortColsAndDirections(List<String> sortColsAndDirections) {
        this.sortColsAndDirections = sortColsAndDirections;
    }
}
