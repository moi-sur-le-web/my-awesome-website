package fr.nico.myawesomewebsite.webapp.dto.fil;

import fr.nico.myawesomewebsite.domaine.referentiel.ActionEnum;

public class FilResumeDto {

    private Long id;

    private String titre;

    private ActionEnum action;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitre() {
        return this.titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public ActionEnum getAction() {
        return action;
    }

    public void setAction(ActionEnum action) {
        this.action = action;
    }
}