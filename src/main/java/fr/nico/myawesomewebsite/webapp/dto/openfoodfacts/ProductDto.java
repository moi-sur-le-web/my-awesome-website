package fr.nico.myawesomewebsite.webapp.dto.openfoodfacts;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Class ProductDto.
 */
public class ProductDto implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3389860589780661090L;

	/** The product name fr. */
	@JsonProperty(value = "product_name_fr")
	private String product_name_fr;
	
	/** The nutriscore grade. */
	@JsonProperty(value = "nutriscore_grade")
	private String nutriscore_grade;
	
	/** The nutriments. */
	@JsonProperty(value = "nutriments")
	private NutimentsDto nutriments;

	/**
	 * Gets the product name fr.
	 *
	 * @return the product name fr
	 */
	public String getProduct_name_fr() {
		return product_name_fr;
	}

	/**
	 * Sets the product name fr.
	 *
	 * @param product_name_fr the new product name fr
	 */
	public void setProduct_name_fr(String product_name_fr) {
		this.product_name_fr = product_name_fr;
	}

	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * Gets the nutriments.
	 *
	 * @return the nutriments
	 */
	public NutimentsDto getNutriments() {
		return nutriments;
	}

	/**
	 * Sets the nutriments.
	 *
	 * @param nutriments the new nutriments
	 */
	public void setNutriments(NutimentsDto nutriments) {
		this.nutriments = nutriments;
	}
}
