package fr.nico.myawesomewebsite.webapp.dto.fil;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import javax.validation.groups.Default;

import org.hibernate.validator.constraints.URL;
import org.springframework.web.multipart.MultipartFile;

public class FilToSaveDto {

    @NotBlank(groups = Default.class)
    @Size(max = 255)
    private String titre;

    @NotBlank()
    private String message;

    @URL()
    private String lien;

    private MultipartFile image;

    private String tags;

    private boolean appercu = Boolean.FALSE;

    public String getTitre() {
        return this.titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLien() {
        return this.lien;
    }

    public void setLien(String lien) {
        this.lien = lien;
    }

    public MultipartFile getImage() {
        return this.image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

    public boolean isAppercu() {
        return this.appercu;
    }

    public void setAppercu(boolean appercu) {
        this.appercu = appercu;
    }

    public String getTags() {
        return this.tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }
}