package fr.nico.myawesomewebsite.webapp.dto;

import java.io.Serializable;

import fr.nico.myawesomewebsite.domaine.referentiel.RythmeEntrainementHebdomadaireEnum;

/**
 * The Class RythmeDto.
 */
public class RythmeDto implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4624087186082946925L;

	/** The id. */
	private Long id;
	
	/** The rythme entrainement hebdomadaire. */
	private RythmeEntrainementHebdomadaireEnum entrainementHebdomadaire;
	
	/** The nb repas jour. */
	private int nbRepasJour;
	
	/** The proteine kilo. */
	private double proteineKilo;
	
	/** The lipide kilo. */
	private double lipideKilo;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the entrainement hebdomadaire.
	 *
	 * @return the entrainement hebdomadaire
	 */
	public RythmeEntrainementHebdomadaireEnum getEntrainementHebdomadaire() {
		return entrainementHebdomadaire;
	}

	/**
	 * Sets the entrainement hebdomadaire.
	 *
	 * @param entrainementHebdomadaire the new entrainement hebdomadaire
	 */
	public void setEntrainementHebdomadaire(RythmeEntrainementHebdomadaireEnum entrainementHebdomadaire) {
		this.entrainementHebdomadaire = entrainementHebdomadaire;
	}

	/**
	 * Gets the nb repas jour.
	 *
	 * @return the nb repas jour
	 */
	public int getNbRepasJour() {
		return nbRepasJour;
	}

	/**
	 * Sets the nb repas jour.
	 *
	 * @param nbRepasJour the new nb repas jour
	 */
	public void setNbRepasJour(int nbRepasJour) {
		this.nbRepasJour = nbRepasJour;
	}

	/**
	 * Gets the proteine kilo.
	 *
	 * @return the proteine kilo
	 */
	public double getProteineKilo() {
		return proteineKilo;
	}

	/**
	 * Sets the proteine kilo.
	 *
	 * @param proteineKilo the new proteine kilo
	 */
	public void setProteineKilo(double proteineKilo) {
		this.proteineKilo = proteineKilo;
	}

	/**
	 * Gets the lipide kilo.
	 *
	 * @return the lipide kilo
	 */
	public double getLipideKilo() {
		return lipideKilo;
	}

	/**
	 * Sets the lipide kilo.
	 *
	 * @param lipideKilo the new lipide kilo
	 */
	public void setLipideKilo(double lipideKilo) {
		this.lipideKilo = lipideKilo;
	}
}
