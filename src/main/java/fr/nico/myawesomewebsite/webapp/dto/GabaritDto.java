package fr.nico.myawesomewebsite.webapp.dto;

/**
 * The Class GabaritDTO.
 */
public class GabaritDto {
    
    /** The id. */
    private Long id;
    
    /** The poids. */
    private double poids;
    
    /** The taille. */
    private int taille;
    
    /** The tour de taille. */
    private double tourDeTaille;
    
    /** The tour de cou. */
    private double tourDeCou;
    
    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }
    
    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * Gets the poids.
     *
     * @return the poids
     */
    public double getPoids() {
        return poids;
    }
    
    /**
     * Sets the poids.
     *
     * @param poids the new poids
     */
    public void setPoids(double poids) {
        this.poids = poids;
    }
    
    /**
     * Gets the taille.
     *
     * @return the taille
     */
    public int getTaille() {
        return taille;
    }
    
    /**
     * Sets the taille.
     *
     * @param taille the new taille
     */
    public void setTaille(int taille) {
        this.taille = taille;
    }
    
    /**
     * Gets the tour de taille.
     *
     * @return the tour de taille
     */
    public double getTourDeTaille() {
        return tourDeTaille;
    }
    
    /**
     * Sets the tour de taille.
     *
     * @param tourDeTaille the new tour de taille
     */
    public void setTourDeTaille(double tourDeTaille) {
        this.tourDeTaille = tourDeTaille;
    }
    
    /**
     * Gets the tour de cou.
     *
     * @return the tour de cou
     */
    public double getTourDeCou() {
        return tourDeCou;
    }
    
    /**
     * Sets the tour de cou.
     *
     * @param tourDeCou the new tour de cou
     */
    public void setTourDeCou(double tourDeCou) {
        this.tourDeCou = tourDeCou;
    }

}
