package fr.nico.myawesomewebsite.webapp.dto.fil;

public class TagDto {

    private String libelle;
    
    private long nbAttributions;

    public String getLibelle() {
        return this.libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public long getNbAttributions() {
        return nbAttributions;
    }

    public void setNbAttributions(long l) {
        this.nbAttributions = l;
    }
}