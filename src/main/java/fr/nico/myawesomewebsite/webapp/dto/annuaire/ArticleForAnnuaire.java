package fr.nico.myawesomewebsite.webapp.dto.annuaire;

public class ArticleForAnnuaire {
    
    private String nom;

    private String description;

    private String lien;
        
    
    /** 
     * @return String
     */
    public String getNom() {
        return nom;
    }

    
    /** 
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    
    /** 
     * @return String
     */
    public String getDescription() {
        return description;
    }

    
    /** 
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }


    public String getLien() {
        return lien;
    }


    public void setLien(String lien) {
        this.lien = lien;
    }
}
