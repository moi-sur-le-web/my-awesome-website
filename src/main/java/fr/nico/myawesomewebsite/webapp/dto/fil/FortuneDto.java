package fr.nico.myawesomewebsite.webapp.dto.fil;

public class FortuneDto {
    
    private String libelle;
    private String auteur;
    
    public String getLibelle() {
        return libelle;
    }
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
    public String getAuteur() {
        return auteur;
    }
    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }
}