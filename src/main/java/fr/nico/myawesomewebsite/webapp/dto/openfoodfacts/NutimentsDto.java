package fr.nico.myawesomewebsite.webapp.dto.openfoodfacts;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Class NutimentsDto.
 */
public class NutimentsDto implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3011369841914001891L;

	/** The proteines. */
	@JsonProperty(value = "proteins_100g")
	private double proteins_100g;
	
	/** The glucides. */
	@JsonProperty(value = "carbohydrates_100g")
	private double carbohydrates_100g;
	
	/** The lipides. */
	@JsonProperty(value = "fat_100g")
	private double fat_100g;
	
	/** The energie. */
	@JsonProperty(value = "energy-kcal_100g")
	private int energy_kcal_100g;
	
	/** The energie 100 g. */
	@JsonProperty(value = "energie_100g")
	private int energie_100g;

	/**
	 * Gets the proteins 100 g.
	 *
	 * @return the proteins 100 g
	 */
	public double getProteins_100g() {
		return proteins_100g;
	}

	/**
	 * Sets the proteins 100 g.
	 *
	 * @param proteins_100g the new proteins 100 g
	 */
	public void setProteins_100g(double proteins_100g) {
		this.proteins_100g = proteins_100g;
	}

	/**
	 * Gets the carbohydrates 100 g.
	 *
	 * @return the carbohydrates 100 g
	 */
	public double getCarbohydrates_100g() {
		return carbohydrates_100g;
	}

	/**
	 * Sets the carbohydrates 100 g.
	 *
	 * @param carbohydrates_100g the new carbohydrates 100 g
	 */
	public void setCarbohydrates_100g(double carbohydrates_100g) {
		this.carbohydrates_100g = carbohydrates_100g;
	}

	/**
	 * Gets the fat 100 g.
	 *
	 * @return the fat 100 g
	 */
	public double getFat_100g() {
		return fat_100g;
	}

	/**
	 * Sets the fat 100 g.
	 *
	 * @param fat_100g the new fat 100 g
	 */
	public void setFat_100g(double fat_100g) {
		this.fat_100g = fat_100g;
	}

	/**
	 * Gets the energy kcal 100 g.
	 *
	 * @return the energy kcal 100 g
	 */
	public int getEnergy_kcal_100g() {
		return energy_kcal_100g;
	}

	/**
	 * Sets the energy kcal 100 g.
	 *
	 * @param energy_kcal_100g the new energy kcal 100 g
	 */
	public void setEnergy_kcal_100g(int energy_kcal_100g) {
		this.energy_kcal_100g = energy_kcal_100g;
	}

	/**
	 * Gets the energie 100 g.
	 *
	 * @return the energie 100 g
	 */
	public int getEnergie_100g() {
		return energie_100g;
	}

	/**
	 * Sets the energie 100 g.
	 *
	 * @param energie_100g the new energie 100 g
	 */
	public void setEnergie_100g(int energie_100g) {
		this.energie_100g = energie_100g;
	}
	
}
