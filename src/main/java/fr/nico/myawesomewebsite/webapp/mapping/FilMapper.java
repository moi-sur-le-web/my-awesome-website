package fr.nico.myawesomewebsite.webapp.mapping;

import java.util.List;

import org.springframework.data.domain.Page;

import fr.nico.myawesomewebsite.domaine.entities.Fil;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilForPageDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilForRubriqueDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilResumeDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilToUpdateDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FortuneDto;

/**
 * The Interface FilMapper.
 */
public interface FilMapper {

    /**
     * Fil to fil for page dto.
     *
     * @param fil the fil
     * @return the fil for page dto
     */
    FilForPageDto filToFilForPageDto(Fil fil);

    /**
     * Fil to fil for page dto.
     *
     * @param fil the fil
     * @return the list
     */
    List<FilForPageDto> filToFilForPageDto(List<Fil> fil);

    /**
     * Fils to page of fils.
     *
     * @param fils the fils
     * @return the page
     */
    Page<FilForPageDto> filsToPageOfFils(Page<Fil> fils);

    /**
     * Fil to fil dto.
     *
     * @param fil the fil
     * @return the fil dto
     */
    FilDto filToFilDto(Fil fil);

    /**
     * Fil to fil to update dto.
     *
     * @param fil the fil
     * @return the fil to update dto
     */
    FilToUpdateDto filToFilToUpdateDto(Fil fil);

    /**
     * Fil to fil resume dto.
     *
     * @param fil the fil to save
     * @return the fil resume dto
     */
    FilResumeDto filToFilResumeDto(Fil fil);

    /**
     * Fil to fil for rubrique dto.
     *
     * @param fil the fil
     * @return the fil for rubrique dto
     */
    FilForRubriqueDto filToFilForRubriqueDto(Fil fil);

    /**
     * Fil to fil for rubrique dto.
     *
     * @param fil the fil
     * @return the list
     */
    List<FilForRubriqueDto> filToFilForRubriqueDto(List<Fil> fil);

    /**
     * Fil to fil for rubrique dto.
     *
     * @param fils the fils
     * @return the page
     */
    Page<FilForRubriqueDto> filToFilForRubriqueDto(Page<Fil> fils);
    
    /**
     * Fil to fortune dto.
     *
     * @param fil the fil
     * @return the fortune dto
     */
    FortuneDto filToFortuneDto(Fil fil);
}