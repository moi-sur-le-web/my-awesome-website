package fr.nico.myawesomewebsite.webapp.mapping.impl;

import org.springframework.stereotype.Component;

import fr.nico.myawesomewebsite.domaine.entities.Gabarit;
import fr.nico.myawesomewebsite.webapp.dto.GabaritDto;
import fr.nico.myawesomewebsite.webapp.mapping.GabaritMapper;

/**
 * The Class GabaritMapperImpl.
 */
@Component
public class GabaritMapperImpl implements GabaritMapper {
    
    /**
     * Gabarit to gabarit dto.
     *
     * @param entity the entity
     * @return the gabarit dto
     */
    public GabaritDto gabaritToGabaritDto(Gabarit entity) {
     
        GabaritDto dto = new GabaritDto();
        
        dto.setId(entity.getId());
        dto.setPoids(entity.getPoids());
        dto.setTaille(entity.getTaille());
        dto.setTourDeCou(entity.getTourDeCou());
        dto.setTourDeTaille(entity.getTourDeTaille());
        
        return dto;
    }

    /**
     * Gabarit dto to gabarit.
     *
     * @param gDto the g dto
     * @return the gabarit
     */
    public Gabarit gabaritDtoToGabarit(GabaritDto gDto) {

        Gabarit entity = new Gabarit();
        
        entity.setId(gDto.getId());
        entity.setPoids(gDto.getPoids());
        entity.setTaille(gDto.getTaille());
        entity.setTourDeCou(gDto.getTourDeCou());
        entity.setTourDeTaille(gDto.getTourDeTaille());
        
        return entity;
    }
    
}
