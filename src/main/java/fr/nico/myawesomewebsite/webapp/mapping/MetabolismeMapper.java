package fr.nico.myawesomewebsite.webapp.mapping;

import java.util.List;
import java.util.Set;

import fr.nico.myawesomewebsite.domaine.entities.Metabolisme;
import fr.nico.myawesomewebsite.webapp.dto.MetabolismeDto;

/**
 * The Interface MetabolismeMapper.
 */
public interface MetabolismeMapper {

    /**
     * Metabolisme to metabolisme dto.
     *
     * @param entity the entity
     * @return the metabolisme dto
     */
    MetabolismeDto metabolismeToMetabolismeDto(Metabolisme entity);
    
    /**
     * Set<Metabolisme> to List<metabolisme dto>.
     *
     * @param entities the entities
     * @return the list
     */
    List<MetabolismeDto> metabolismeToMetabolismeDto(Set<Metabolisme> entities);

    /**
     * Metabolisme to metabolisme dto.
     *
     * @param metabolismesBdd the metabolismes bdd
     * @return the list
     */
    List<MetabolismeDto> metabolismeToMetabolismeDto(List<Metabolisme> metabolismesBdd);

    /**
     * Metabolisme dto to metabolisme.
     *
     * @param metabolismeDto the metabolisme dto
     * @return the metabolisme
     */
    Metabolisme metabolismeDtoToMetabolisme(MetabolismeDto metabolismeDto);

    /**
     * Metabolisme dto to metabolisme.
     *
     * @param metabolismesDto the metabolismes dto
     * @return the sets the
     */
    Set<Metabolisme> metabolismeDtoToMetabolisme(List<MetabolismeDto> metabolismesDto);
    
}
