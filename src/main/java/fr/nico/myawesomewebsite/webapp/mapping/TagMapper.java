package fr.nico.myawesomewebsite.webapp.mapping;

import java.util.List;

import fr.nico.myawesomewebsite.domaine.entities.Tag;
import fr.nico.myawesomewebsite.webapp.dto.fil.TagDto;

/**
 * The Interface TagMapper.
 */
public interface TagMapper {

    /**
     * Tag to tag dto.
     *
     * @param tag the tag
     * @return the tag dto
     */
    TagDto tagToTagDto(Tag tag);

    /**
     * Tag to tag dto.
     *
     * @param tags the tags
     * @return the list
     */
    List<TagDto> tagToTagDto(List<Tag> tags);
}
