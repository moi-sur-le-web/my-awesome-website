package fr.nico.myawesomewebsite.webapp.mapping;

import fr.nico.myawesomewebsite.domaine.entities.Rythme;
import fr.nico.myawesomewebsite.webapp.dto.RythmeDto;

/**
 * The Interface RythmeMapper.
 */
public interface RythmeMapper {

	/**
	 * Rythme to rythme dto.
	 *
	 * @param rythme the rythme
	 * @return the rythme dto
	 */
	RythmeDto rythmeToRythmeDto(Rythme rythme);

	/**
	 * Rythme dto to rythme.
	 *
	 * @param rythmeDto the rythme dto
	 * @return the rythme
	 */
	Rythme rythmeDtoToRythme(RythmeDto rythmeDto);
}
