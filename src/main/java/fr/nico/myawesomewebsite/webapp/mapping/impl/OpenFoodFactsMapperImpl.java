package fr.nico.myawesomewebsite.webapp.mapping.impl;

import org.springframework.stereotype.Component;

import fr.nico.myawesomewebsite.domaine.entities.Ingredient;
import fr.nico.myawesomewebsite.webapp.dto.IngredientDto;
import fr.nico.myawesomewebsite.webapp.dto.openfoodfacts.OpenFoodFactsResponseDto;
import fr.nico.myawesomewebsite.webapp.mapping.OpenFoodFactsMapper;

/**
 * The Class OpenFoodFactsMapperImpl.
 */
@Component
public class OpenFoodFactsMapperImpl implements OpenFoodFactsMapper {
	
	/** The kilo joules to calories. */
	private static double kiloJoulesToCalories = 4.184;
	
	/**
	 * Open food facts response dto to aliment dto.
	 *
	 * @param source the source
	 * @param codeBarre the code barre
	 * @return the ingredient dto
	 */
	@Override
	public IngredientDto openFoodFactsResponseDtoToAlimentDto(OpenFoodFactsResponseDto source, String codeBarre) {
		
		if (source == null) {
			return null;
		}
		
		IngredientDto target = new IngredientDto();
		target.setCodeBarre(codeBarre);
		target.setNom(source.getProductDto().getProduct_name_fr());
		target.setProteines(source.getProductDto().getNutriments().getProteins_100g());
		target.setGlucides(source.getProductDto().getNutriments().getCarbohydrates_100g());
		target.setLipides(source.getProductDto().getNutriments().getFat_100g());
		
		// l'énergie provient du champ energie_100g (en kJ) ou du champ Energy_kcal_100g (en Cal)
		if (source.getProductDto().getNutriments().getEnergie_100g() != 0) {
			target.setEnergie((int) (source.getProductDto().getNutriments().getEnergie_100g() / kiloJoulesToCalories));
		} else {
			target.setEnergie(source.getProductDto().getNutriments().getEnergy_kcal_100g());
		}
		
		return target;
	}

	/**
	 * Ingredient to ingredient dto.
	 *
	 * @param source the source
	 * @return the ingredient dto
	 */
	@Override
	public IngredientDto ingredientToIngredientDto(Ingredient source) {
		
		if (source == null) {
			return null;
		}
		
		IngredientDto target = new IngredientDto();
		target.setId(source.getId());
		target.setNom(source.getNom());
		target.setGlucides(source.getGlucides());
		target.setLipides(source.getLipides());
		target.setProteines(source.getProteines());
		target.setEnergie(source.getEnergie());
		return target;
	}

	/**
	 * Ingredient dto to ingredient.
	 *
	 * @param source the source
	 * @return the ingredient
	 */
	@Override
	public Ingredient ingredientDtoToIngredient(IngredientDto source) {
		
		if (source == null) {
			return null;
		}
		
		Ingredient target = new Ingredient();
		target.setId(source.getId());
		target.setNom(source.getNom());
		target.setCodeBarre(source.getCodeBarre());
		target.setGlucides(source.getGlucides());
		target.setLipides(source.getLipides());
		target.setProteines(source.getProteines());
		target.setEnergie(source.getEnergie());
		return target;
	}

}
