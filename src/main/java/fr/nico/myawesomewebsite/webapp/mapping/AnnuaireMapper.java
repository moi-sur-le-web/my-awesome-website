package fr.nico.myawesomewebsite.webapp.mapping;

import java.util.List;

import org.springframework.data.domain.Page;

import fr.nico.myawesomewebsite.domaine.entities.Fil;
import fr.nico.myawesomewebsite.webapp.dto.annuaire.ArticleForAnnuaire;

public interface AnnuaireMapper {
    
    ArticleForAnnuaire articleToArticleForAnnuaireDto(Fil article);

    List<ArticleForAnnuaire> articleToArticleForAnnuaireDto(List<Fil> articles);

    Page<ArticleForAnnuaire> articleToArticleForAnnuaireDto(Page<Fil> articles);
}
