package fr.nico.myawesomewebsite.webapp.mapping.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import fr.nico.myawesomewebsite.domaine.entities.Utilisateur;
import fr.nico.myawesomewebsite.webapp.dto.ProfilUtilisateurDto;
import fr.nico.myawesomewebsite.webapp.dto.UtilisateurAgeDTO;
import fr.nico.myawesomewebsite.webapp.dto.UtilisateurDto;
import fr.nico.myawesomewebsite.webapp.mapping.GabaritMapper;
import fr.nico.myawesomewebsite.webapp.mapping.MetabolismeMapper;
import fr.nico.myawesomewebsite.webapp.mapping.RythmeMapper;
import fr.nico.myawesomewebsite.webapp.mapping.UtilisateurMapper;

/**
 * The Class UtilisateurMapperImpl.
 */
@Component
public class UtilisateurMapperImpl implements UtilisateurMapper {

    /** The gabarit mapper. */
    @Autowired
    private GabaritMapper gabaritMapper;

    /** The metabolisme mapper. */
    @Autowired
    private MetabolismeMapper metabolismeMapper;

    /** The rythme mapper. */
    @Autowired
    private RythmeMapper rythmeMapper;

    /**
     * Utilisateur to utilisateur dto.
     *
     * @param utilisateur the utilisateur
     * @return the utilisateur DTO
     */
    @Override
    public UtilisateurDto utilisateurToUtilisateurDto( Utilisateur utilisateur ) {

        UtilisateurDto uDto = new UtilisateurDto();

        uDto.setId(utilisateur.getId());
        uDto.setNom(utilisateur.getNom());
        uDto.setPrenom(utilisateur.getPrenom());
        uDto.setSexe(utilisateur.getSexe());
        uDto.setAnneeDeNaissance(utilisateur.getAnneeDeNaissance());
        uDto.setMail(utilisateur.getMail());
        uDto.setProfil(utilisateur.getProfil());

        if (utilisateur.getGabarit() != null) {
            uDto.setGabaritDto(this.gabaritMapper.gabaritToGabaritDto(utilisateur.getGabarit()));
        }

        if (utilisateur.getMetabolismes() != null) {
            uDto.setMetabolismesDto(this.metabolismeMapper.metabolismeToMetabolismeDto(utilisateur.getMetabolismes()));
        }

        if (utilisateur.getRythme() != null) {
            uDto.setRythmeDto(this.rythmeMapper.rythmeToRythmeDto(utilisateur.getRythme()));
        }

        return uDto;
    }

    /**
     * Utilisateur dto to utilisateur.
     *
     * @param source the utilisateur DTO
     * @return the utilisateur
     */
    @Override
    public Utilisateur utilisateurDtoToUtilisateur( UtilisateurDto source ) {

        Utilisateur target = new Utilisateur();

        target.setId(source.getId());
        target.setMail(source.getMail());
        target.setPrenom(source.getPrenom());
        target.setNom(source.getNom());
        target.setSexe(source.getSexe());
        target.setAnneeDeNaissance(source.getAnneeDeNaissance());
        target.setMotDePasse(source.getMotDePasse());
        target.setProfil(source.getProfil());

        if (source.getGabaritDto() != null) {
            target.setGabarit(this.gabaritMapper.gabaritDtoToGabarit(source.getGabaritDto()));
        }

        if (source.getRythmeDto() != null) {
            target.setRythme(this.rythmeMapper.rythmeDtoToRythme(source.getRythmeDto()));
        }

        if (source.getMetabolismesDto() != null) {
            target.setMetabolismes(this.metabolismeMapper.metabolismeDtoToMetabolisme(source.getMetabolismesDto()));
        }

        return target;
    }

    /**
     * Utilisateur to utilisateur dto.
     *
     * @param uListe the u liste
     * @return the list
     */
    @Override
    public List<UtilisateurDto> utilisateurToUtilisateurDto( Iterable<Utilisateur> uListe ) {

        List<UtilisateurDto> uDtos = new ArrayList<>();
        for (Utilisateur u : uListe) {
            uDtos.add(this.utilisateurToUtilisateurDto(u));
        }
        return uDtos;
    }

    /**
     * To dto.
     *
     * @param uListe the u liste
     * @return the list
     */
    @Override
    public List<UtilisateurDto> toDto(List<Utilisateur> uListe) {

        List<UtilisateurDto> uDtos = new ArrayList<>();
        for (Utilisateur u : uListe) {
            uDtos.add(this.utilisateurToUtilisateurDto(u));
        }
        return uDtos;
    }

    /**
     * To dto.
     *
     * @param uEntityPage the u entity page
     * @return the page
     */
    public Page<UtilisateurDto> toDto(Page<Utilisateur> uEntityPage) {

        List<UtilisateurDto> uDtoList = new ArrayList<>();
        for ( Utilisateur entity : uEntityPage ) {
            uDtoList.add(this.utilisateurToUtilisateurDto(entity));
        }
        return new PageImpl<>(uDtoList, uEntityPage.getPageable(), uEntityPage.getTotalElements());
    }

    /**
     * Utilisateur dto to utilisateur age dto.
     *
     * @param utilisateurDto the utilisateur dto
     * @return the utilisateur age DTO
     */
    @Override
    public UtilisateurAgeDTO utilisateurDtoToUtilisateurAgeDto(UtilisateurDto utilisateurDto) {

        UtilisateurAgeDTO utiAge = new UtilisateurAgeDTO();
        utiAge.setId(utilisateurDto.getId());
        utiAge.setNom(utilisateurDto.getNom());
        utiAge.setPrenom(utilisateurDto.getPrenom());
        utiAge.setSexe(utilisateurDto.getSexe());
        utiAge.setAge(Calendar.getInstance().get(Calendar.YEAR) - utilisateurDto.getAnneeDeNaissance());
        utiAge.setMail(utilisateurDto.getMail());
        utiAge.setMotDePasse(utilisateurDto.getMotDePasse());
        utiAge.setProfil(utilisateurDto.getProfil());
        utiAge.setGabaritDto(utilisateurDto.getGabaritDto());
        utiAge.setMetabolismesDto(utilisateurDto.getMetabolismesDto());
        utiAge.setRythmeDto(utilisateurDto.getRythmeDto());

        return utiAge;
    }

    /**
     * Utilisateur to utilisateur.
     *
     * @param utilisateurToSave the utilisateur to save
     * @param utilisateurBdd the utilisateur bdd
     * @return the utilisateur
     */
    @Override
    public Utilisateur miseAJourUtilisateur(Utilisateur utilisateurToSave, Utilisateur utilisateurBdd) {

        utilisateurToSave.setId(utilisateurBdd.getId());
        utilisateurToSave.setMotDePasse(utilisateurBdd.getMotDePasse());
        return utilisateurToSave;
    }

    @Override
    public Utilisateur nouvelUtilisateurDtoToUtilisateur(ProfilUtilisateurDto source) {
        Utilisateur target = new Utilisateur();
        target.setNom(source.getNom());
        target.setPrenom(source.getPrenom());
        target.setSexe(source.getSexe());
        target.setMail(source.getMail());
        target.setMotDePasse(source.getMotDePasse());
        target.setAnneeDeNaissance(source.getAnneeDeNaissance());
        return target;
    }

    @Override
    public ProfilUtilisateurDto utilisateurToProfilUtilisateurDto(Utilisateur source) {
        
        if (source == null) {
            return null;
        }
        ProfilUtilisateurDto target = new ProfilUtilisateurDto();
        target.setPrenom(source.getPrenom());
        target.setNom(source.getNom());
        target.setAnneeDeNaissance(source.getAnneeDeNaissance());
        target.setSexe(source.getSexe());
        target.setMail(source.getMail());
        return target;
    }
}
