package fr.nico.myawesomewebsite.webapp.mapping;

import fr.nico.myawesomewebsite.domaine.entities.Gabarit;
import fr.nico.myawesomewebsite.webapp.dto.GabaritDto;

/**
 * The Interface GabaritMapper.
 */
public interface GabaritMapper {

    /**
     * Gabarit to gabarit dto.
     *
     * @param entity the entity
     * @return the gabarit dto
     */
    GabaritDto gabaritToGabaritDto(Gabarit entity);

    /**
     * Gabarit dto to gabarit.
     *
     * @param gDto the g dto
     * @return the gabarit
     */
    Gabarit gabaritDtoToGabarit(GabaritDto gDto);

}
