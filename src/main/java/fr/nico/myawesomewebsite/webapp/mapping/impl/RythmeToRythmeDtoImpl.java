package fr.nico.myawesomewebsite.webapp.mapping.impl;

import org.springframework.stereotype.Component;

import fr.nico.myawesomewebsite.domaine.entities.Rythme;
import fr.nico.myawesomewebsite.webapp.dto.RythmeDto;
import fr.nico.myawesomewebsite.webapp.mapping.RythmeMapper;

/**
 * The Class RythmeToRythmeDtoImpl.
 */
@Component
public class RythmeToRythmeDtoImpl implements RythmeMapper {

	/**
	 * Rythme to rythme dto.
	 *
	 * @param source the source
	 * @return the rythme dto
	 */
	@Override
	public RythmeDto rythmeToRythmeDto(Rythme source) {
		
		if (source == null) {
			return null;
		}
		
		RythmeDto target = new RythmeDto();
		target.setId(source.getId());
		target.setEntrainementHebdomadaire(source.getEntrainementHebdomadaire());
		target.setNbRepasJour(source.getNbRepasJour());
		target.setProteineKilo(source.getProteineKilo());
		target.setLipideKilo(source.getLipideKilo());
		
		return target;
	}

	/**
	 * Rythme dto to rythme.
	 *
	 * @param source the rythme dto
	 * @return the rythme
	 */
	@Override
	public Rythme rythmeDtoToRythme(RythmeDto source) {
		
		if (source == null) {
			return null;
		}
		
		Rythme target = new Rythme();
		target.setId(source.getId());
		target.setEntrainementHebdomadaire(source.getEntrainementHebdomadaire());
		target.setNbRepasJour(source.getNbRepasJour());
		target.setProteineKilo(source.getProteineKilo());
		target.setLipideKilo(source.getLipideKilo());
		
		return target;
	}

}
