package fr.nico.myawesomewebsite.webapp.mapping.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import fr.nico.myawesomewebsite.domaine.entities.Tag;
import fr.nico.myawesomewebsite.webapp.dto.fil.TagDto;
import fr.nico.myawesomewebsite.webapp.mapping.TagMapper;

@Component
public class TagMapperImpl implements TagMapper {

    @Override
    public TagDto tagToTagDto(Tag source) {
        if (source == null) {
            return null;
        }
        TagDto target = new TagDto();
        target.setLibelle(source.getLibelle());
        target.setNbAttributions(source.getNbAttributions());
        return target;
    }

    @Override
    public List<TagDto> tagToTagDto(List<Tag> source) {
        if (source.isEmpty()) {
            return new ArrayList<>();
        }
        List<TagDto> target = new ArrayList<>();
        for (Tag tag : source) {
            target.add(this.tagToTagDto(tag));
        }
        return target;
    }

}
