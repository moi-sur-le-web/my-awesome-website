package fr.nico.myawesomewebsite.webapp.mapping.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import fr.nico.myawesomewebsite.domaine.entities.Fil;
import fr.nico.myawesomewebsite.webapp.dto.annuaire.ArticleForAnnuaire;
import fr.nico.myawesomewebsite.webapp.mapping.AnnuaireMapper;

@Component
public class AnnuaireMapperImpl implements AnnuaireMapper {

    @Override
    public ArticleForAnnuaire articleToArticleForAnnuaireDto(Fil source) {
        
        if (source == null){
            return null;
        }
        ArticleForAnnuaire target = new ArticleForAnnuaire();
        target.setNom(source.getTitre());
        target.setDescription(source.getHtmlMessage());
        target.setLien(source.getLien());
        return target;
    }

    @Override
    public List<ArticleForAnnuaire> articleToArticleForAnnuaireDto(List<Fil> source) {
        if (source.isEmpty()) {
            return Collections.emptyList();
        }
        List<ArticleForAnnuaire> target = new ArrayList<>();
        for(Fil article : source){
            target.add(this.articleToArticleForAnnuaireDto(article));
        }
        return target;
    }

    @Override
    public Page<ArticleForAnnuaire> articleToArticleForAnnuaireDto(Page<Fil> source) {
        List<ArticleForAnnuaire> target = this.articleToArticleForAnnuaireDto(source.getContent());
        return new PageImpl<>(target, source.getPageable(), source.getTotalElements());
    }
}