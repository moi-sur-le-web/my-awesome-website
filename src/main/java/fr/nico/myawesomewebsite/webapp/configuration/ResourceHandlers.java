package fr.nico.myawesomewebsite.webapp.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class ResourceHandlers implements WebMvcConfigurer {

    private Logger logger = LoggerFactory.getLogger(ResourceHandlers.class);

    @Value("${file.upload.directory}")
    private String fileUploadDirectory;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        // https://www.baeldung.com/run-shell-command-in-java
        String os = System.getProperty("os.name").toLowerCase();
        boolean isLinux = os.startsWith("linux");
        this.logger.info("Système d'exploitation : {}", isLinux ? "Linux" : "Windows");

        String locationPrefix = "file:";
        if (!isLinux) {
            // https://www.baeldung.com/spring-mvc-static-resources#2-serving-a-resource-stored-in-the-file-system
            locationPrefix = locationPrefix.concat("///");
        }
        registry.addResourceHandler("/files/**").addResourceLocations(locationPrefix.concat(this.fileUploadDirectory));
        this.logger.info("Répertoire de téléversement des images : {}", locationPrefix.concat(this.fileUploadDirectory));

        // En cas de surcharge du chemin de sauvegarde des images, ne pas oublier le "/" final.

        registry
                .addResourceHandler("/webjars/**")
                .addResourceLocations("/webjars/");
    }
}
