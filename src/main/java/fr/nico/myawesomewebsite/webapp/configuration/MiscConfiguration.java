package fr.nico.myawesomewebsite.webapp.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class MiscConfiguration {

    private Logger logger = LoggerFactory.getLogger(MiscConfiguration.class);

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
        this.logger.info("Instanciation du restTemplateBuilder");
        return restTemplateBuilder.build();
    }
}