package fr.nico.myawesomewebsite.webapp.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.nico.myawesomewebsite.metier.BlogService;
import fr.nico.myawesomewebsite.webapp.dto.PageRequestDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilForPageDto;


/**
 * The Class FilRestController.
 */
@RestController
@RequestMapping(value = "/fil")
public class FilRestController {

    /** The fil service. */
    @Autowired
    private BlogService filService;

    /**
     * Page fil.
     *
     * @param pageRequestDto the page request dto
     * @return the response entity
     */
    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @CrossOrigin
    public ResponseEntity<Page<FilForPageDto>> pageFil(PageRequestDto pageRequestDto) {
        Page<FilForPageDto> pageOfFils = this.filService.rechercherFils(pageRequestDto, null);
        if (pageOfFils.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(pageOfFils, HttpStatus.OK);
    }
}
