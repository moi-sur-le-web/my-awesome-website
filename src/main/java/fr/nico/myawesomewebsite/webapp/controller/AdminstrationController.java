package fr.nico.myawesomewebsite.webapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.nico.myawesomewebsite.metier.UtilisateurService;
import fr.nico.myawesomewebsite.webapp.dto.UtilisateurDto;

/**
 * The Class AdminstrationController.
 */
@Controller
@RequestMapping(value = "/administration")
public class AdminstrationController {
	
	/** The utilisateur service. */
    @Autowired
    private UtilisateurService utilisateurService;
    
    /**
     * Lister utilisateurs.
     *
     * @param model the model
     * @return the string
     */
    @GetMapping(value = { "/liste" })
    public String listerUtilisateurs(Model model) {

        List<UtilisateurDto> uDtos = utilisateurService.getListeUtilisateurs();
        model.addAttribute("utilisateurs", uDtos);
        model.addAttribute("page", "utilisateur/liste.html");

        return "layouts/model";
    }
}
