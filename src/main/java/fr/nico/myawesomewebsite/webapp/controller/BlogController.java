package fr.nico.myawesomewebsite.webapp.controller;

import java.io.IOException;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.nico.myawesomewebsite.domaine.entities.Utilisateur;
import fr.nico.myawesomewebsite.domaine.exception.FilServiceException;
import fr.nico.myawesomewebsite.metier.FichierService;
import fr.nico.myawesomewebsite.metier.TagService;
import fr.nico.myawesomewebsite.metier.BlogService;
import fr.nico.myawesomewebsite.metier.UtilisateurService;
import fr.nico.myawesomewebsite.webapp.dto.PageRequestDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilForPageDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilResumeDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilToSaveDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilToUpdateDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.OrigineDto;

/**
 * The Class BlogController.
 */
@Controller
@RequestMapping(value = "/fil")
public class BlogController {

    private static final String UTILISATEUR_NON_AUTHENTIFIE_RETOUR_PAGE_ACCUEIL = "Utilisateur non authentifié, retour à la page d'accueil.";

    /** The logger. */
    private Logger logger = LoggerFactory.getLogger(BlogController.class);

    /** The Constant MESSAGE_ECHEC. */
    private static final String MESSAGE_ECHEC = "messageEchec";

    /** The Constant FIL_RESUME_DTO. */
    private static final String FIL_RESUME_DTO = "filResumeDto";

    /** The Constant IMAGE. */
    private static final String IMAGE = "image";

    /** The Constant FIL_NOUVEAU. */
    private static final String FIL_NOUVEAU = "fil/nouveau";

    /** The Constant FIL_EDITER. */
    private static final String FIL_EDITER = "fil/editer";

    /** The Constant TEMPLATE. */
    private static final String TEMPLATE = "layouts/model";

    /** The Constant REDIRECT_PAGE_PRINCIPALE. */
    private static final String REDIRECT_PAGE_PRINCIPALE = "redirect:/fil/page-principale";

    /** The Constant REDIRECT_CONSULTER. */
    private static final String REDIRECT_CONSULTER = "redirect:/fil/consulter";

    /** The Constant REDIRECT_FIL_RUBRIQUE_NOM. */
    private static final String REDIRECT_FIL_RUBRIQUE_NOM = "redirect:/fil/rubrique?nom=";
    
    /** The Constant REDIRECT_HOME. */
    private static final String REDIRECT_HOME = "redirect:/";

    /** The Constant AND_SIZE_EQUALS. */
    private static final String AND_SIZE_EQUALS = "&size=";
    
    /** The Constant AND_NUMBER_EQUALS. */
    private static final String AND_NUMBER_EQUALS = "&number=";
    
    /** The fil service. */
    @Autowired
    private BlogService filService;

    /** The fichier service. */
    @Autowired
    private FichierService fichierService;

    /** The utilisateur service. */
    @Autowired
    private UtilisateurService utilisateurService;
    
    @Autowired
    private TagService tagService;

    /**
     * Page fil.
     *
     * @param model the model
     * @param auth the auth
     * @param pageResquestDto the page resquest dto
     * @return the string
     */
    @GetMapping(value = "/page-principale")
    public String pageFil(Model model, Authentication auth, PageRequestDto pageResquestDto) {
        this.logger.info("Ouverture du fil - données d'entrée : {}", model);
        Page<FilForPageDto> pageOfFils = this.filService.rechercherFils(pageResquestDto, auth);
        model.addAttribute("pageOfObjects", pageOfFils);
        model.addAttribute("page", "fil/page-principale");
        return TEMPLATE;
    }

    /**
     * Formulaire nouveau fil.
     *
     * @param model the model
     * @param auth the auth
     * @return the string
     */
    @GetMapping(value = "/nouveau")
    public String formulaireNouveauFil(Model model, Authentication auth) {
        this.logger.info("Enregistrement d'un nouveau fil. Données d'entrée : {}", model);
        if (auth != null) {
            model.addAttribute("fil", new FilToSaveDto());
            model.addAttribute("page", FIL_NOUVEAU);
            return TEMPLATE;
        }
        this.logger.error(UTILISATEUR_NON_AUTHENTIFIE_RETOUR_PAGE_ACCUEIL);
        return REDIRECT_PAGE_PRINCIPALE;
    }

    /**
     * Enregistrer nouveau fil.
     *
     * @param model the model
     * @param redirect the redirect
     * @param authentication the authentication
     * @param filToSaveDto the fil to save dto
     * @param bindingResult the binding result
     * @return the string
     */
    @PostMapping(value = "/enregister")
    public String enregistrerNouveauFil(Model model, RedirectAttributes redirect, Authentication authentication, @Valid FilToSaveDto filToSaveDto,
            BindingResult bindingResult) {
        this.logger.info("Enregistrement du nouveau fil. Données d'entrée : {}.", model);
        this.validerFichierImage(filToSaveDto, bindingResult);
        if (bindingResult.hasErrors()) {
            this.filService.setModelFromBinding(model, bindingResult);
            model.addAttribute("fil", filToSaveDto);
            model.addAttribute("page", FIL_NOUVEAU);
            return TEMPLATE;
        }
        try {
            Utilisateur auteur = this.utilisateurService.findByAuthenticationName(authentication.getName());
            FilResumeDto filResumeDto = this.filService.enregistrerFil(auteur, filToSaveDto);
            redirect.addFlashAttribute(FIL_RESUME_DTO, filResumeDto);
            this.logger.info("Fil ID #{} enregistré avec succès.", filResumeDto.getId());
        } catch (FilServiceException e) {
            this.logger.error(e.getMessage());
            redirect.addFlashAttribute(MESSAGE_ECHEC, e.getMessage());
        }
        return REDIRECT_PAGE_PRINCIPALE;
    }

    /**
     * Supprimer fil.
     *
     * @param redirect the redirect
     * @param auth the auth
     * @param origineDto the origine dto
     * @return the string
     */
    @PostMapping(value = "/supprimer")
    public String supprimerFil(RedirectAttributes redirect, Authentication auth, OrigineDto origineDto) {
        this.logger.info("Tentative de suppression du fil #ID {}.", origineDto.getId());
        boolean suppressionReussie = true;
        try {
            FilResumeDto filResumeDto = this.filService.supprimerFil(auth, origineDto.getId());
            redirect.addFlashAttribute(FIL_RESUME_DTO, filResumeDto);
            this.logger.info("Suppression du fil #ID {} réussie.", origineDto.getId());
        } catch (FilServiceException e) {
            redirect.addFlashAttribute(MESSAGE_ECHEC, e.getMessage());
            this.logger.error("Suppression du fil ID #{} impossible.", origineDto.getId());
            suppressionReussie = false;
        }

        if (suppressionReussie) {
            String[] ecransOrigine = origineDto.getEcranOrigine().split(",");
            if (ecransOrigine[0].equals("page-principale") || origineDto.getEcranOrigine().equals("consulter")) {
                return REDIRECT_PAGE_PRINCIPALE + "?number=" + origineDto.getNumber() + AND_SIZE_EQUALS + origineDto.getSize();
            } else {
                return REDIRECT_FIL_RUBRIQUE_NOM + origineDto.getEcranOrigine() + AND_NUMBER_EQUALS + origineDto.getNumber() + AND_SIZE_EQUALS
                        + origineDto.getSize();
            }

        }
        return REDIRECT_PAGE_PRINCIPALE;
    }

    /**
     * Consulter detail.
     *
     * @param model the model
     * @param redirect the redirect
     * @param auth the auth
     * @param origineDto the origine dto
     * @return the string
     */
    @GetMapping(value = "/consulter")
    public String consulterDetail(Model model, RedirectAttributes redirect, Authentication auth, OrigineDto origineDto) {
        this.logger.info("Ouverture du fil ID #{} en mode consultation.", origineDto.getId());
        FilDto fil;
        try {
            fil = this.filService.rechercherFil(auth, origineDto.getId());
            model.addAttribute("fil", fil);
            model.addAttribute("origine", origineDto);
        } catch (FilServiceException e) {
            this.logger.error(e.getMessage());
            redirect.addFlashAttribute(MESSAGE_ECHEC, e.getMessage());
            return REDIRECT_PAGE_PRINCIPALE;
        }
        model.addAttribute("page", "fil/consulter");
        return TEMPLATE;
    }

    /**
     * Editer.
     *
     * @param model the model
     * @param redirect the redirect
     * @param authentication the authentication
     * @param origine the origine
     * @return the string
     */
    @GetMapping(value = "/editer")
    public String editer(Model model, RedirectAttributes redirect, Authentication authentication, OrigineDto origine) {
        this.logger.info("Tentative d'édition du fil #ID {}.", origine.getId());
        try {
            model.addAttribute("fil", this.filService.rechercherPourEdition(authentication, origine));
        } catch (FilServiceException e) {
            this.logger.error(e.getMessage());
            redirect.addFlashAttribute(MESSAGE_ECHEC, e.getMessage());
            return REDIRECT_PAGE_PRINCIPALE;
        }
        model.addAttribute("page", FIL_EDITER);
        return TEMPLATE;
    }

    /**
     * Mettre A jour.
     *
     * @param model the model
     * @param redirect the redirect
     * @param authentication the authentication
     * @param filToUpdateDto the fil to update dto
     * @param bindingResult the binding result
     * @return the string
     */
    @PostMapping(value = "/mettre-a-jour")
    public String mettreAJour(Model model, RedirectAttributes redirect, Authentication authentication, @Valid FilToUpdateDto filToUpdateDto,
            BindingResult bindingResult) {

        this.validerFichierImage(filToUpdateDto, bindingResult);
        if (bindingResult.hasErrors()) {
            this.logger.warn("Erreur(s) dans le formulaire de modification du fil #ID {} : {} ", filToUpdateDto.getOrigine().getId(), bindingResult);
            this.filService.setModelFromBinding(model, bindingResult);
            model.addAttribute("fil", filToUpdateDto);
            model.addAttribute("page", FIL_NOUVEAU);
            return TEMPLATE;
        }

        boolean saveSuccess = true;
        try {
            FilResumeDto filResumeDto = this.filService.mettreAJourFil(authentication, filToUpdateDto);
            redirect.addFlashAttribute(FIL_RESUME_DTO, filResumeDto);
        } catch (IOException | FilServiceException e) {
            this.logger.error(e.getMessage());
            redirect.addFlashAttribute(MESSAGE_ECHEC, e.getMessage());
            saveSuccess = false;
        }

        if (saveSuccess) {
            String[] ecrans = filToUpdateDto.getOrigine().getEcranOrigine().split(",");
            if (ecrans[ecrans.length - 1].equals("page-principale")) {
                return this.redirectToPagePrincipale(filToUpdateDto);
            } else if (ecrans[ecrans.length - 1].equals("consulter")) {
                return this.redirectToConsulter(filToUpdateDto);
            } else {
                return this.redirectToRubrique(filToUpdateDto);
            }
        }

        return REDIRECT_PAGE_PRINCIPALE;
    }

    /**
     * Redirect to consulter.
     *
     * @param filToUpdateDto the fil to update dto
     * @return the string
     */
    //@formatter:off
    private String redirectToConsulter(FilToUpdateDto filToUpdateDto) {
        return REDIRECT_CONSULTER + "?id=" + filToUpdateDto.getOrigine().getId()
                + "&libelleOrigine=" + filToUpdateDto.getOrigine().getEcranOrigine().split(",")[0]
                + AND_NUMBER_EQUALS + filToUpdateDto.getOrigine().getNumber()
                + AND_SIZE_EQUALS + filToUpdateDto.getOrigine().getSize();
    }

    /**
     * Redirect to rubrique.
     *
     * @param filToUpdateDto the fil to update dto
     * @return the string
     */
    private String redirectToRubrique(FilToUpdateDto filToUpdateDto) {
        return REDIRECT_FIL_RUBRIQUE_NOM + filToUpdateDto.getOrigine().getEcranOrigine()
                + AND_NUMBER_EQUALS + filToUpdateDto.getOrigine().getNumber()
                + AND_SIZE_EQUALS + filToUpdateDto.getOrigine().getSize()
                + "#" + filToUpdateDto.getOrigine().getId();
    }

    /**
     * Redirect to page principale.
     *
     * @param filToUpdateDto the fil to update dto
     * @return the string
     */
    private String redirectToPagePrincipale(FilToUpdateDto filToUpdateDto) {
        return REDIRECT_PAGE_PRINCIPALE
                + "?number=" + filToUpdateDto.getOrigine().getNumber()
                + AND_SIZE_EQUALS + filToUpdateDto.getOrigine().getSize()
                + "#" + filToUpdateDto.getOrigine().getId();
    }
    // @formatter:on

    /**
     * Valider fichier image.
     *
     * @param filToSaveDto the fil to save dto
     * @param bindingResult the binding result
     */
    private void validerFichierImage(FilToSaveDto filToSaveDto, BindingResult bindingResult) {
        boolean imageValide = true;
        if (!filToSaveDto.getImage().isEmpty()) {
            try {
                imageValide = this.fichierService.checkFileMediaType(filToSaveDto, IMAGE);
            } catch (IOException e) {
                imageValide = false;
                this.logger.error(e.getMessage());
            }
        }
        if (!imageValide) {
            bindingResult.addError(new FieldError("fil", IMAGE, "Fichier corrompu ou mauvais type."));
        }
    }

    /**
     * Rubrique.
     *
     * @param model the model
     * @param auth the auth
     * @param pageResquestDto the page resquest dto
     * @param nom the nom
     * @return the string
     */
    @GetMapping(value = "rubrique")
    public String rubrique(Model model, Authentication auth, PageRequestDto pageResquestDto, @RequestParam String nom) {
        if (nom.equals("accueil")) {
            this.logger.info("Retour à la page d'accueil");
            return REDIRECT_HOME;
        }
        this.logger.info("Ouverture de la rubrique [{}] données d'entrée : {}", nom, pageResquestDto);
        Page<FilForPageDto> fils;
        try {
            fils = this.filService.rechercherFilsPourRubrique(auth, nom, pageResquestDto);
            model.addAttribute("rubrique", nom);
            model.addAttribute("pageOfObjects", fils);
            model.addAttribute("page", "fil/rubrique");
        } catch (FilServiceException e) {
            this.logger.error(e.getMessage());
            return REDIRECT_HOME;
        }
        return TEMPLATE;
    }
    
    @GetMapping(value = "mur-tags")
    public String murTags(Model model) {
        model.addAttribute("tags", this.tagService.murTags());
        model.addAttribute("page", "fil/mur-tags");
        return TEMPLATE;                
    }

}