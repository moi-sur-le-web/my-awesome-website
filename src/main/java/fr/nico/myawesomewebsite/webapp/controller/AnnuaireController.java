package fr.nico.myawesomewebsite.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import fr.nico.myawesomewebsite.metier.AnnuaireService;
import fr.nico.myawesomewebsite.webapp.dto.PageRequestDto;

@Controller
public class AnnuaireController {
    
    @Autowired
    private AnnuaireService annuaireService;

    @GetMapping(value = "/annuaire")
    public String annuaire(Model model, PageRequestDto pageRequestDto) {
        
        model.addAttribute("pageOfObjects", this.annuaireService.getAnnuaire(pageRequestDto));
        model.addAttribute("page", "annuaire/annuaire");
        return "layouts/model";
    }
}
