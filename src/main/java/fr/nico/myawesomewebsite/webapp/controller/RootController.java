package fr.nico.myawesomewebsite.webapp.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.nico.myawesomewebsite.domaine.exception.FilServiceException;
import fr.nico.myawesomewebsite.domaine.exception.TechniqueException;
import fr.nico.myawesomewebsite.metier.BlogService;
import fr.nico.myawesomewebsite.metier.TechniqueService;
import fr.nico.myawesomewebsite.metier.UtilisateurService;
import fr.nico.myawesomewebsite.webapp.dto.ProfilUtilisateurDto;
import fr.nico.myawesomewebsite.webapp.dto.UtilisateurDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilForPageDto;

/**
 * The Class RootController.
 */
@Controller
public class RootController {

    /** The logger. */
    private Logger logger = LoggerFactory.getLogger(RootController.class);

    /** The Constant NOUVEL_UTILISATEUR. */
    private static final String PROFIL_UTILISATEUR = "profilUtilisateur";

    /** The Constant LAYOUTS_MODEL. */
    private static final String LAYOUTS_MODEL = "layouts/model";
    
    /** The Constant FORTUNE_ECHEC. */
    private static final String FORTUNE_ECHEC = "fortuneEchec";
   
    /** The Constant EDITO_ECHEC. */
    private static final String EDITO_ECHEC = "editoEchec";

    /** The Constant IMAGE_MOMENT_ECHEC. */
    private static final String IMAGE_MOMENT_ECHEC = "imageMomentEchec";

    /** The utilisateur service. */
    @Autowired
    private UtilisateurService utilisateurService;
    
    /** The blog service. */
    @Autowired
    private BlogService blogService;
    
    /** The technique service. */
    @Autowired
    private TechniqueService techniqueService;

    /**
     * Home.
     *
     * @param auth the auth
     * @param model the model
     * @return the string
     */
    @GetMapping(value = { "/" })
    public String home(Authentication auth, Model model) {
        
        // Fortune
        if (techniqueService.isLinux()) {
            try {
                model.addAttribute("fortune", blogService.getFortuneOfTheDay());
                model.addAttribute(FORTUNE_ECHEC, false);
            } catch (TechniqueException | FilServiceException e) {
                this.logger.error(e.getMessage());
                model.addAttribute(FORTUNE_ECHEC, true);
            }            
        } else {
            this.logger.error("L'OS n'est pas Linux. Impossible d'exécuter le programme Fortune.");
            model.addAttribute(FORTUNE_ECHEC, true);            
        }
        
        // Edito
        try {
            model.addAttribute("edito", this.blogService.regchercherPlusRecentArticleParAdmin("edito"));       
            model.addAttribute(EDITO_ECHEC, false);       
        } catch (FilServiceException e) {
            this.logger.error("Aucun edito trouvé.");
            model.addAttribute(EDITO_ECHEC, true);            
        }        
        
        // Image du moment
        try {
            FilForPageDto imageDuMoment = this.blogService.regchercherPlusRecentArticleParAdmin("image-du-moment");
            this.blogService.contientImage(imageDuMoment);
            model.addAttribute("imageDuMoment", this.blogService.regchercherPlusRecentArticleParAdmin("image-du-moment"));       
            model.addAttribute(IMAGE_MOMENT_ECHEC, false);       
        } catch (FilServiceException e) {
            this.logger.error("Aucune Image du moment valide trouvée.");
            model.addAttribute(IMAGE_MOMENT_ECHEC, true);            
        }          
        
        model.addAttribute("page", "accueil");
        return LAYOUTS_MODEL;
    }

    /**
     * Login.
     *
     * @param model the model
     * @return the string
     */
    @GetMapping(value = "/connexion")
    public String login(Model model) {
        model.addAttribute(PROFIL_UTILISATEUR, new ProfilUtilisateurDto());
        model.addAttribute("page", "connexion");
        return LAYOUTS_MODEL;
    }

    /**
     * Login failed.
     *
     * @param model the model
     * @return the string
     */
    @GetMapping(value = { "/loginFailure" })
    public String loginFailed(RedirectAttributes model) {

        model.addFlashAttribute("loginFailure", true);
        return "redirect:/connexion";
    }

    /**
     * Sign up.
     *
     * @param model the model
     * @param redirect the redirect
     * @param utilisateurDto the utilisateur dto
     * @param bindingResult the binding result
     * @return the string
     */
    @PostMapping(value = { "/enregistrer" })
    public String signUp(Model model, RedirectAttributes redirect, @Valid @ModelAttribute ProfilUtilisateurDto utilisateurDto, BindingResult bindingResult) {

        List<String> errors = this.utilisateurService.validerNouvelUtilisateur(utilisateurDto, bindingResult);
        if (!errors.isEmpty()) {
            model.addAttribute("errors", errors);
            model.addAttribute(PROFIL_UTILISATEUR, utilisateurDto);
            model.addAttribute("page", "connexion");
            return LAYOUTS_MODEL;
        }
        this.utilisateurService.inscrireUtilisateur(utilisateurDto);
        redirect.addFlashAttribute("enregistrementOk", true);
        return "redirect:/";
    }

    /** 
     * @param model
     * @param auth
     * @return String
     */
    @GetMapping(value = "/profil")
    public String editerProfil(Model model, Authentication auth) {
        if (model.containsAttribute(PROFIL_UTILISATEUR)) {
            model.addAttribute(PROFIL_UTILISATEUR, model.getAttribute(PROFIL_UTILISATEUR));
        } else {
            try {
                model.addAttribute(PROFIL_UTILISATEUR, this.utilisateurService.getProfilUtilisateur(auth));
            } catch (TechniqueException e) {
                this.logger.error(e.getMessage());
                return "redirect:/";
            }
        }
        model.addAttribute("page", "utilisateur/profil");
        return LAYOUTS_MODEL;
    }

    @PostMapping(value = "/mettre-a-jour")
    public String update(Authentication auth, Model model, RedirectAttributes redirect, @Valid @ModelAttribute ProfilUtilisateurDto profilUtilisateurDto, BindingResult bindingResult) {
        
        try {
            List<String> errors = this.utilisateurService.validerMiseAJourProfilUtilisateur(auth, profilUtilisateurDto, bindingResult);
            if (!errors.isEmpty()) {
                redirect.addFlashAttribute("errors", errors);
                redirect.addFlashAttribute(PROFIL_UTILISATEUR, profilUtilisateurDto);
                return "redirect:/profil";
            }
        } catch (TechniqueException e) {
            this.logger.error(e.getMessage());
            return "redirect:/";
        }
        
        try {
            UtilisateurDto utilisateurDto = this.utilisateurService.updateProfilUtilisateur(auth, profilUtilisateurDto);
            Authentication newAuth = new UsernamePasswordAuthenticationToken(utilisateurDto.getMail(), utilisateurDto.getMotDePasse());
            SecurityContextHolder.getContext().setAuthentication(newAuth);
            redirect.addFlashAttribute("utilisateurDto", utilisateurDto);
        } catch (TechniqueException e) {
            this.logger.error(e.getMessage());
            return "redirect:/";
        }
        return "redirect:/profil";
    }
}
