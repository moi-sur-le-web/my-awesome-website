package fr.nico.myawesomewebsite.webapp.controller;

import java.util.Comparator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import fr.nico.myawesomewebsite.domaine.exception.OpenFoodFactsServiceException;
import fr.nico.myawesomewebsite.metier.OpenFoodFactsService;
import fr.nico.myawesomewebsite.metier.TableauDeBordService;
import fr.nico.myawesomewebsite.metier.UtilisateurService;
import fr.nico.myawesomewebsite.webapp.dto.GabaritDto;
import fr.nico.myawesomewebsite.webapp.dto.IngredientDto;
import fr.nico.myawesomewebsite.webapp.dto.MetabolismeDto;
import fr.nico.myawesomewebsite.webapp.dto.RythmeDto;
import fr.nico.myawesomewebsite.webapp.dto.UtilisateurAgeDTO;
import fr.nico.myawesomewebsite.webapp.dto.UtilisateurDto;
import fr.nico.myawesomewebsite.webapp.mapping.UtilisateurMapper;

/**
 * The Class SportNutritionController.
 */
@Controller
@RequestMapping(value = "/sport-nutrition")
public class SportNutritionController {

	/** The Constant TEMPLATE. */
	private static final String TEMPLATE = "layouts/model";

	/** The Constant TABLEAU_DE_BORD. */
	private static final String TABLEAU_DE_BORD = "redirect:/sport-nutrition/tableau-de-bord";

	/** The Constant OPEN_FOOD_FACTS. */
	private static final String OPEN_FOOD_FACTS = "redirect:/sport-nutrition/open-food-facts";

	/** The Constant NOM. */
	private static final String NOM = "?nom=";

	/** The Constant PROTEINES. */
	private static final String PROTEINES = "&proteines=";

	/** The Constant CODE. */
	private static final String CODE = "&code=";
	
	/** The Constant LIPIDES. */
	private static final String LIPIDES = "&lipides=";

	/** The Constant GLUCIDES. */
	private static final String GLUCIDES = "&glucides=";

	/** The Constant ENERGIE. */
	private static final String ENERGIE = "&energie=";

	/** The Constant ERREUR. */
	private static final String ERREUR = "?erreur=";

	/** The tableau de bord service. */
	@Autowired
	private TableauDeBordService tableauDeBordService;

	/** The utilisateur service. */
	@Autowired
	private UtilisateurService utilisateurService;

	/** The utilisateur mapper. */
	@Autowired
	private UtilisateurMapper utilisateurMapper;

	/** The open food facts service. */
	@Autowired
	private OpenFoodFactsService openFoodFactsService;
	
	@Value("${message.erreur.format.code.barre}")
	private String erreurFormatCodeBarre;

	/**
	 * Afficher tableau de bord.
	 *
	 * @param model the model
	 * @param auth  the auth
	 * @return the string
	 */
	@GetMapping(value = { "/tableau-de-bord" })
	public String afficherTableauDeBord(Model model, Authentication auth) {

		UtilisateurDto utilisateurDto = this.utilisateurService.rechercherUtilisateurToutesDonnees(auth.getName());
		UtilisateurAgeDTO utilisateurAgeDto = this.utilisateurMapper.utilisateurDtoToUtilisateurAgeDto(utilisateurDto);
		model.addAttribute("utilisateur", utilisateurAgeDto);

		if (utilisateurAgeDto.getGabaritDto() != null) {
			model.addAttribute("gabarit", utilisateurAgeDto.getGabaritDto());
		}
		if (utilisateurAgeDto.getRythmeDto() != null) {
			model.addAttribute("rythme", utilisateurAgeDto.getRythmeDto());
		}
		if (utilisateurAgeDto.getMetabolismesDto() != null) {
			model.addAttribute("parValeurMetabolisme", Comparator.comparing(MetabolismeDto::getValeur));
			model.addAttribute("metabolismes", utilisateurAgeDto.getMetabolismesDto());
		}
		model.addAttribute("page", "openFoodFacts/model.html");
		model.addAttribute("sousPage", "openFoodFacts/tableau-de-bord");

		return TEMPLATE;
	}

	/**
	 * Initialiser gabarit.
	 *
	 * @param auth the auth
	 * @return the string
	 */
	@PostMapping(value = { "/initialiser-gabarit" })
	public String initialiserGabarit(Authentication auth) {

		UtilisateurDto utilisateurDto = this.utilisateurService.rechercherUtilisateur(auth.getName());
		this.tableauDeBordService.initialiserGabarit(utilisateurDto);
		return TABLEAU_DE_BORD;
	}

	/**
	 * Enregistrer gabarit.
	 *
	 * @param auth    the auth
	 * @param gabarit the gabarit
	 * @return the string
	 */
	@PostMapping(value = { "/enregistrer-gabarit" })
	public String enregistrerGabarit(Authentication auth, @ModelAttribute(name = "gabarit") GabaritDto gabarit) {

		UtilisateurDto utilisateurDto = this.utilisateurService.rechercherUtilisateur(auth.getName());
		this.tableauDeBordService.actualiserGabarit(utilisateurDto, gabarit);
		return TABLEAU_DE_BORD;
	}

	/**
	 * Initialiser rythme.
	 *
	 * @param auth the auth
	 * @return the string
	 */
	@PostMapping(value = { "/initialiser-rythme" })
	public String initialiserRythme(Authentication auth) {

		UtilisateurDto utilisateurDto = this.utilisateurService.rechercherUtilisateur(auth.getName());
		this.tableauDeBordService.initialiserRythme(utilisateurDto);
		return TABLEAU_DE_BORD;
	}

	/**
	 * Enregistrer rythme.
	 *
	 * @param auth      the auth
	 * @param model     the model
	 * @param rythmeDto the rythme dto
	 * @return the string
	 */
	@PostMapping(value = { "/enregistrer-rythme" })
	public String enregistrerRythme(Authentication auth, Model model,
			@ModelAttribute(name = "rythme") RythmeDto rythmeDto) {

		UtilisateurDto utilisateurDto = this.utilisateurService.rechercherUtilisateur(auth.getName());
		this.tableauDeBordService.actualiserRythme(utilisateurDto, rythmeDto);
		return TABLEAU_DE_BORD;
	}

	/**
	 * Rechercher item open food facts.
	 *
	 * @param model     the model
	 * @param nom       the nom
	 * @param proteines the proteines
	 * @param lipides   the lipides
	 * @param glucides  the glucides
	 * @param energie   the energie
	 * @param erreur the erreur
	 * @return the string
	 */
	@GetMapping(value = "/open-food-facts")
	public String openFoodFacts(Model model, @RequestParam(required = false) String nom,
			@RequestParam(required = false) String code,
			@RequestParam(required = false) Double proteines, @RequestParam(required = false) Double lipides,
			@RequestParam(required = false) Double glucides, @RequestParam(required = false) Integer energie,
			@RequestParam(required = false) String erreur) {

		if (nom != null && code != null && proteines != null && lipides != null && glucides != null && energie != null) {
			IngredientDto ingredientDto = new IngredientDto();
			ingredientDto.setNom(nom);
			ingredientDto.setCodeBarre(code);
			ingredientDto.setProteines(proteines);
			ingredientDto.setLipides(lipides);
			ingredientDto.setGlucides(glucides);
			ingredientDto.setEnergie(energie);
			model.addAttribute("ingredientDto", ingredientDto);
		}

		if (erreur != null) {
			model.addAttribute("erreur", erreur);
		}

		model.addAttribute("page", "openFoodFacts/model.html");
		model.addAttribute("sousPage", "openFoodFacts/open-food-facts.html");
		return TEMPLATE;
	}

	/**
	 * Rechercher article open food fact.
	 *
	 * @param codeBarre the code barre
	 * @return the string
	 */
	@PostMapping(value = { "/rechercher-item" })
	public String rechercherArticleOpenFoodFact(@RequestParam String codeBarre, Model model) {

		IngredientDto ingredientDto = new IngredientDto();
		StringBuilder url = new StringBuilder();

		try {
			ingredientDto = this.openFoodFactsService.rechercherValeursNutritionnellesOpenFoodFacts(codeBarre);
		} catch (OpenFoodFactsServiceException openFoodFactsException) {
			url.append(OPEN_FOOD_FACTS + ERREUR + openFoodFactsException.getMessage());
			return url.toString();
		}
		
		url.append(OPEN_FOOD_FACTS + NOM + ingredientDto.getNom());
		url.append(CODE + ingredientDto.getCodeBarre());
		url.append(PROTEINES + ingredientDto.getProteines());
		url.append(LIPIDES + ingredientDto.getLipides());
		url.append(GLUCIDES + ingredientDto.getGlucides());
		url.append(ENERGIE + ingredientDto.getEnergie());
		
		return  url.toString();
	}
	
	@PostMapping(value = {"/enregistrer-item"})
	public String enregistrerItemOpenFoodFact(@ModelAttribute(name = "ingredientDto") IngredientDto openFoodFactsItemDto) {
	
		this.openFoodFactsService.enregistrer(openFoodFactsItemDto);
		return OPEN_FOOD_FACTS;
	}
}
