package fr.nico.myawesomewebsite.metier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.nico.myawesomewebsite.domaine.entities.Fil;
import fr.nico.myawesomewebsite.persistance.AnnuaireSpecification;
import fr.nico.myawesomewebsite.persistance.FilRepository;
import fr.nico.myawesomewebsite.webapp.dto.PageRequestDto;
import fr.nico.myawesomewebsite.webapp.dto.annuaire.ArticleForAnnuaire;
import fr.nico.myawesomewebsite.webapp.mapping.AnnuaireMapper;

@Service
public class AnnuaireService {

    @Autowired
    private FilRepository filRepository;

    @Autowired
    private AnnuaireMapper annuaireMapper;

    @Autowired
    private PaginationService paginationService;
    
    /** 
     * @param pageRequestDto
     * @return Page<Fil>
     */
    public Page<ArticleForAnnuaire> getAnnuaire(PageRequestDto pageRequestDto){
        
        this.setOrderByIfNone(pageRequestDto);
        Pageable pageable = this.paginationService.forgePageRequest(pageRequestDto, Fil.class);
        Page<Fil> articles = this.filRepository.findAll(new AnnuaireSpecification(), pageable);
        return this.annuaireMapper.articleToArticleForAnnuaireDto(articles);
    }

    private void setOrderByIfNone(PageRequestDto dto) {
        if (dto.getSortColsAndDirections().isEmpty()){
            dto.getSortColsAndDirections().add("titre:asc");
        }
    }
}
