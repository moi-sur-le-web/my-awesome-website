package fr.nico.myawesomewebsite.metier;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import fr.nico.myawesomewebsite.domaine.entities.Fil;
import fr.nico.myawesomewebsite.domaine.entities.Tag;
import fr.nico.myawesomewebsite.domaine.entities.Utilisateur;
import fr.nico.myawesomewebsite.domaine.exception.FilServiceException;
import fr.nico.myawesomewebsite.domaine.exception.TechniqueException;
import fr.nico.myawesomewebsite.domaine.referentiel.ActionEnum;
import fr.nico.myawesomewebsite.domaine.referentiel.ProfilEnum;
import fr.nico.myawesomewebsite.persistance.ArticleSpecification;
import fr.nico.myawesomewebsite.persistance.FilRepository;
import fr.nico.myawesomewebsite.persistance.RubriqueSpecification;
import fr.nico.myawesomewebsite.webapp.dto.PageRequestDto;
import fr.nico.myawesomewebsite.webapp.dto.UtilisateurDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilForPageDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilResumeDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilToSaveDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilToUpdateDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FortuneDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.OrigineDto;
import fr.nico.myawesomewebsite.webapp.mapping.FilMapper;

/**
 * The Class BlogService.
 */
@Service
@Transactional
public class BlogService {

    /** The logger. */
    Logger logger = LoggerFactory.getLogger(BlogService.class);

    /** The Constant OUVERTURE_DU_FIL_ID_IMPOSSIBLE. */
    private static final String OUVERTURE_DU_FIL_ID_IMPOSSIBLE = "Ouverture du fil id #{} impossible";

    /** The Constant IMPOSSIBLE_ENREGISTRER_NOUVEAU_FIL. */
    private static final String IMPOSSIBLE_ENREGISTRER_NOUVEAU_FIL = "Erreur : Auteur introuvable - Impossible d'enregistrer le nouveau fil.";

    /** The Constant ERREUR_MESSAGE_EST_OBLIGATOIRE. */
    private static final String ERREUR_MESSAGE_EST_OBLIGATOIRE = "Erreur : message est obligatoire !";

    /** The Constant ERREUR_TITRE_EST_OBLIGATOIRE. */
    private static final String ERREUR_TITRE_EST_OBLIGATOIRE = "Erreur : titre est obligatoire !";

    /** The Constant IMPOSSIBLE_SUPPRIMER_FIL. */
    private static final String IMPOSSIBLE_SUPPRIMER_FIL = "Impossible de supprimer le fil";

    /** The Constant IMPOSSIBLE_METTRE_A_JOUR_FIL. */
    private static final String IMPOSSIBLE_METTRE_A_JOUR_FIL = "Impossible de mettre à jour le fil !";

    /** The Constant FIL_INTROUVABLE. */
    private static final String FIL_INTROUVABLE = "Fil introuvable en base de données !";

    /** The Constant SYSTEM_AT_DUMMY_FR. */
    private static final String SYSTEM_AT_DUMMY_FR = "system@dummy.fr";
    
    /** The Constant IMPOSSIBLE_OBTENIR_FORTUNE_DU_JOUR. */
    private static final String IMPOSSIBLE_OBTENIR_FORTUNE_DU_JOUR = "Impossible d'obtenir une fortune du jour.";

    /** The Constant AUCUN_EDITO_TROUVE. */
    private static final String AUCUN_EDITO_TROUVE = "Aucun edito trouvé.";
    
    /** The fil repository. */
    @Autowired
    private FilRepository filRepository;

    /** The fil mapper. */
    @Autowired
    private FilMapper filMapper;

    /** The file service. */
    @Autowired
    private FichierService fileService;

    /** The utilisateur service. */
    @Autowired
    private UtilisateurService utilisateurService;

    /** The pagination service. */
    @Autowired
    private PaginationService paginationService;

    /** The auth service. */
    @Autowired
    private AuthService authService;

    /** The simple mark down service. */
    @Autowired
    private SimpleMarkDownService simpleMarkDownService;

    /** The tag service. */
    @Autowired
    private TagService tagService;

    /** The system service. */
    @Autowired
    private SystemService systemService;
    
    /** The technique service. */
    @Autowired
    private TechniqueService techniqueService;

    /**
     * Supprimer fil.
     *
     * @param auth the auth
     * @param id the id
     * @return the fil resume dto
     * @throws FilServiceException the fil service exception
     */
    public FilResumeDto supprimerFil(Authentication auth, Long id) throws FilServiceException {
        Utilisateur auteur = this.utilisateurService.findByAuthenticationName(auth.getName());
        if (auteur == null) {
            throw new FilServiceException(IMPOSSIBLE_SUPPRIMER_FIL + " : auteur null.");
        }
        Fil fil = null;
        if (auteur.getProfil() == ProfilEnum.ADMIN) {
            fil = this.filRepository.findById(id).orElse(null);
        } else {
            fil = this.filRepository.findByIdAndAuteur(id, auteur).orElse(null);
        }
        if (fil == null) {
            throw new FilServiceException(IMPOSSIBLE_SUPPRIMER_FIL + " : fil introuvable.");
        }
        this.filRepository.delete(fil);

        FilResumeDto filResumeDto = this.filMapper.filToFilResumeDto(fil);
        filResumeDto.setAction(ActionEnum.SUPPRIMER);
        return filResumeDto;
    }

    /**
     * Enregistrer fil.
     *
     * @param auteur the auteur
     * @param filToSaveDto the fil to save dto
     * @return the fil resume dto
     * @throws FilServiceException the fil service exception
     */
    public FilResumeDto enregistrerFil(Utilisateur auteur, FilToSaveDto filToSaveDto) throws FilServiceException {
        if (auteur == null) {
            throw new FilServiceException(IMPOSSIBLE_ENREGISTRER_NOUVEAU_FIL);
        }
        Fil filToSave = new Fil();
        filToSave.setTitre(filToSaveDto.getTitre());
        filToSave.setMessage(filToSaveDto.getMessage());
        filToSave.setHtmlMessage(this.simpleMarkDownService.markdownToHTML(filToSaveDto.getMessage()));
        filToSave.setLien(filToSaveDto.getLien());
        filToSave.setAppercu(filToSaveDto.isAppercu());
        this.setAuteur(auteur, filToSave);
        this.setCreationDate(filToSave);
        this.setMiseAJourDate(filToSave, filToSave.getDateCreation());
        this.saveImage(auteur, filToSaveDto, filToSave);
        this.tagService.saveTags(filToSave, filToSaveDto);

        filToSave = this.filRepository.save(filToSave);

        FilResumeDto f = this.filMapper.filToFilResumeDto(filToSave);
        f.setAction(ActionEnum.ENREGISTRER);
        return f;
    }

    /**
     * Mettre A jour fil.
     *
     * @param authentication the authentication
     * @param filToUpdateDto the fil to update dto
     * @return the fil resume dto
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws FilServiceException the fil service exception
     */
    public FilResumeDto mettreAJourFil(Authentication authentication, FilToUpdateDto filToUpdateDto) throws IOException, FilServiceException {
        Utilisateur auteur = this.utilisateurService.findByAuthenticationName(authentication.getName());
        if (auteur == null) {
            throw new FilServiceException(IMPOSSIBLE_METTRE_A_JOUR_FIL);
        }

        Fil filToUpdate = this.filRepository.findByIdAndAuteur(filToUpdateDto.getOrigine().getId(), auteur).orElse(null);
        if (filToUpdate == null) {
            throw new FilServiceException(FIL_INTROUVABLE);
        }

        filToUpdate.setDateMiseAJour(LocalDateTime.now());

        this.updateTitre(filToUpdate, filToUpdateDto);
        this.setMessages(filToUpdate, filToUpdateDto);
        this.setLien(filToUpdate, filToUpdateDto);
        filToUpdate.setAppercu(filToUpdateDto.isAppercu());

        if (filToUpdateDto.isSupprimerImageActuelle()) {
            this.supprimerImage(filToUpdate);
        }

        if (!filToUpdateDto.getImage().isEmpty()) {
            this.updateImage(auteur, filToUpdateDto, filToUpdate);
        }

        this.tagService.updateTags(filToUpdateDto, filToUpdate);

        filToUpdate = this.filRepository.save(filToUpdate);

        FilResumeDto f = this.filMapper.filToFilResumeDto(filToUpdate);
        f.setAction(ActionEnum.MIS_A_JOUR);
        return f;
    }

    /**
     * Update titre.
     *
     * @param filToUpdate the fil to update
     * @param filToUpdateDto the fil to update dto
     * @throws FilServiceException the fil service exception
     */
    private void updateTitre(Fil filToUpdate, FilToUpdateDto filToUpdateDto) throws FilServiceException {
        if ((filToUpdateDto.getTitre() == null) || filToUpdateDto.getTitre().isEmpty() || filToUpdateDto.getTitre().isBlank()) {
            throw new FilServiceException(ERREUR_TITRE_EST_OBLIGATOIRE);
        }
        filToUpdate.setTitre(filToUpdateDto.getTitre());
    }

    /**
     * Sets the messages.
     *
     * @param filToUpdate the fil to update
     * @param filToUpdateDto the fil to update dto
     * @throws FilServiceException the fil service exception
     */
    private void setMessages(Fil filToUpdate, FilToUpdateDto filToUpdateDto) throws FilServiceException {
        if ((filToUpdateDto.getMessage() == null) || filToUpdateDto.getMessage().isEmpty() || filToUpdateDto.getMessage().isBlank()) {
            throw new FilServiceException(ERREUR_MESSAGE_EST_OBLIGATOIRE);
        }
        filToUpdate.setMessage(filToUpdateDto.getMessage());
        filToUpdate.setHtmlMessage(this.simpleMarkDownService.markdownToHTML(filToUpdateDto.getMessage()));
    }

    /**
     * Sets the lien.
     *
     * @param filToUpdate the fil to update
     * @param filToUpdateDto the fil to update dto
     */
    private void setLien(Fil filToUpdate, FilToUpdateDto filToUpdateDto) {
        if (filToUpdateDto.getLien() != null) {
            filToUpdate.setLien(filToUpdateDto.getLien());
        }
    }

    /**
     * Supprimer image.
     *
     * @param filToUpdate the fil to update
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void supprimerImage(Fil filToUpdate) throws IOException {
        if (filToUpdate.getNomImage() != null) {
            this.fileService.deleteImage(filToUpdate);
            filToUpdate.setNomImage(null);
        }
    }

    /**
     * Update image.
     *
     * @param auteur the auteur
     * @param filToUpdateDto the fil to update dto
     * @param filToUpdate the fil to update
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void updateImage(Utilisateur auteur, FilToUpdateDto filToUpdateDto, Fil filToUpdate) throws IOException {
        this.supprimerImage(filToUpdate);
        String imageName = this.fileService.updateImageFile(auteur, filToUpdateDto, filToUpdate);
        if (imageName != null) {
            filToUpdate.setNomImage(imageName);
        }
    }

    /**
     * Sets the creation date.
     *
     * @param filToSave the new creation date
     */
    private void setCreationDate(Fil filToSave) {
        filToSave.setDateCreation(LocalDateTime.now());
    }

    /**
     * Sets the mise A jour date.
     *
     * @param filToSave the fil to save
     * @param date the date
     */
    private void setMiseAJourDate(Fil filToSave, LocalDateTime date) {
        filToSave.setDateMiseAJour(date);
    }

    /**
     * Save image.
     *
     * @param auteur the auteur
     * @param filToSaveDto the fil to save dto
     * @param filToSave the fil to save
     */
    private void saveImage(Utilisateur auteur, FilToSaveDto filToSaveDto, Fil filToSave) {
        if ((filToSaveDto.getImage() != null) && !filToSaveDto.getImage().isEmpty()) {
            String imageName = this.fileService.saveImageFile(auteur, filToSaveDto);
            if (imageName != null) {
                filToSave.setNomImage(imageName);
            }
        }
    }

    /**
     * Sets the auteur.
     *
     * @param auteur the auteur
     * @param filToSave the fil to save
     * @throws FilServiceException the fil service exception
     */
    private void setAuteur(Utilisateur auteur, Fil filToSave) throws FilServiceException {
        if (auteur == null) {
            throw new FilServiceException(IMPOSSIBLE_ENREGISTRER_NOUVEAU_FIL);
        }
        filToSave.setAuteur(auteur);
    }

    /**
     * Rechercher fils.
     *
     * @param pageResquestDto the page resquest dto
     * @param auth the auth
     * @return the page
     */
    public Page<FilForPageDto> rechercherFils(PageRequestDto pageResquestDto, Authentication auth) {
        this.logger.info("Ouverture du fil - page : {}", pageResquestDto);
        Specification<Fil> specification = new ArticleSpecification(pageResquestDto, this.techniqueService.getFieldsList(Fil.class));
        Pageable pageRequest = this.paginationService.forgePageRequest(pageResquestDto, Fil.class);
        Page<Fil> fils = this.filRepository.findAll(specification, pageRequest);
        if (fils.isEmpty()) {
            return Page.empty();
        }
        Page<FilForPageDto> pageOfFils = this.filMapper.filsToPageOfFils(fils);
        this.setSupprimable(auth, pageOfFils);
        this.setEditable(auth, pageOfFils);
        this.setAlreadyEdited(pageOfFils);
        return pageOfFils;
    }

    /**
     * Rechercher fil.
     *
     * @param auth the auth
     * @param id the id
     * @return the fil dto
     * @throws FilServiceException the fil service exception
     */
    public FilDto rechercherFil(Authentication auth, Long id) throws FilServiceException {
        if ((id == null) || (id <= 0)) {
            this.logger.error(OUVERTURE_DU_FIL_ID_IMPOSSIBLE, id);
            throw new FilServiceException("Fil inéxistant.");
        }
        Fil fil = this.filRepository.findById(id).orElse(null);
        if (fil == null) {
            this.logger.error(OUVERTURE_DU_FIL_ID_IMPOSSIBLE, id);
            throw new FilServiceException("Fil inéxistant.");
        }
        FilDto filDto = this.filMapper.filToFilDto(fil);
        this.setSupprimable(auth, filDto);
        this.setEditable(auth, filDto);
        this.setAlreadyEdited(filDto);
        return filDto;
    }

    /**
     * Sets the supprimable.
     *
     * @param auth the auth
     * @param fil the fil
     */
    private void setSupprimable(Authentication auth, FilDto fil) {
        if (auth != null) {
            UtilisateurDto utilisateurDto = this.utilisateurService.rechercherUtilisateur(auth.getName());
            if ((fil.getAuteurId() == utilisateurDto.getId()) || this.authService.isAdminstrateur(auth)) {
                fil.setSupprimable(true);
            }
        }
    }

    /**
     * Sets the supprimable.
     *
     * @param auth the auth
     * @param pageOfFils the page of fils
     */
    private void setSupprimable(Authentication auth, Page<FilForPageDto> pageOfFils) {
        if (auth != null) {
            UtilisateurDto utilisateurDto = this.utilisateurService.rechercherUtilisateur(auth.getName());
            for (FilForPageDto fil : pageOfFils.getContent()) {
                if ((fil.getAuteurId() == utilisateurDto.getId()) || this.authService.isAdminstrateur(auth)) {
                    fil.setSupprimable(true);
                }
            }
        }
    }

    /**
     * Sets the editable.
     *
     * @param auth the auth
     * @param pageOfFils the page of fils
     */
    private void setEditable(Authentication auth, Page<FilForPageDto> pageOfFils) {
        if (auth != null) {
            UtilisateurDto utilisateurDto = this.utilisateurService.rechercherUtilisateur(auth.getName());
            for (FilForPageDto fil : pageOfFils.getContent()) {
                if ((fil.getAuteurId() == utilisateurDto.getId())) {
                    fil.setEditable(true);
                }
            }
        }
    }

    /**
     * Sets the editable.
     *
     * @param auth the auth
     * @param filDto the fil dto
     */
    private void setEditable(Authentication auth, FilDto filDto) {
        if (auth != null) {
            UtilisateurDto utilisateurDto = this.utilisateurService.rechercherUtilisateur(auth.getName());
            if (filDto.getAuteurId() == utilisateurDto.getId()) {
                filDto.setEditable(true);
            }
        }
    }

    /**
     * Sets the already edited.
     *
     * @param pageOfFils the new already edited
     */
    private void setAlreadyEdited(Page<FilForPageDto> pageOfFils) {
        // place le boolean edited à vrai si le fil à été créé puis ensuite édité
        for (FilForPageDto fil : pageOfFils.getContent()) {
            if (fil.getDateMiseAJour().isAfter(fil.getDateCreation())) {
                fil.setEdited(true);
            }
        }
    }
    
    private void setAlreadyEdited(FilDto filDto) {
        if (filDto.getDateMiseAJour().isAfter(filDto.getDateCreation())) {
            filDto.setEdited(true);
        }
    }

    /**
     * Rechercher pour edition.
     *
     * @param auth the auth
     * @param origine the origine
     * @return the fil to update dto
     * @throws FilServiceException the fil service exception
     */
    public FilToUpdateDto rechercherPourEdition(Authentication auth, OrigineDto origine) throws FilServiceException {
        Utilisateur auteur = this.utilisateurService.findByAuthenticationName(auth.getName());
        if (auteur == null) {
            throw new FilServiceException("Auteur [null] non autorisé pour édition fil #ID " + origine.getId() + ".");
        }
        Fil fil = this.filRepository.findByIdAndAuteur(origine.getId(), auteur).orElse(null);
        if (fil == null) {
            throw new FilServiceException(
                    "Auteur #ID " + auteur.getId() + " non autorisé pour édition fil #ID " + origine.getId() + " ou fil supprimé.");
        }

        FilToUpdateDto filToUpdateDto = this.filMapper.filToFilToUpdateDto(fil);
        filToUpdateDto.setOrigine(origine);
        // tags - 1er jet
        if (!fil.getTags().isEmpty()) {
            String tags = "";
            for (Tag tag : fil.getTags()) {
                tags = tags.concat(tag.getLibelle() + ",");
            }
            filToUpdateDto.setTags(tags);
        }
        // tags - 1er jet
        return filToUpdateDto;
    }

    /**
     * Sets the model from binding.
     *
     * @param model the model
     * @param bindingResult the binding result
     */
    public void setModelFromBinding(Model model, BindingResult bindingResult) {
        if (bindingResult.hasFieldErrors("titre")) {
            model.addAttribute("titreError", "");
        }
        if (bindingResult.hasFieldErrors("message")) {
            model.addAttribute("messageError", "");
        }
        if (bindingResult.hasFieldErrors("lien")) {
            model.addAttribute("lienError", "");
        }
        if (bindingResult.hasFieldErrors("image")) {
            model.addAttribute("imageError", "");
        }
    }

    /**
     * Rechercher fils pour rubrique.
     *
     * @param auth the auth
     * @param rubrique the rubrique
     * @param pageResquestDto the page resquest dto
     * @return the page
     * @throws FilServiceException 
     */
    public Page<FilForPageDto> rechercherFilsPourRubrique(Authentication auth, String rubrique, PageRequestDto pageResquestDto) throws FilServiceException {
        if ("en-cours".equals(rubrique) && auth == null) {
            throw new FilServiceException("Impossible d'afficher ses articles en cours d'écriture sans être authentifié.");
        }
        Pageable pageRequest = this.paginationService.forgePageRequest(pageResquestDto, Fil.class);
        ArticleSpecification spec = new ArticleSpecification(auth, rubrique, pageResquestDto, this.techniqueService.getFieldsList(Fil.class));
        Page<Fil> fils = this.filRepository.findAll(spec, pageRequest);
        if (fils.isEmpty()) {
            return Page.empty();
        }
        Page<FilForPageDto> pageOfFils = this.filMapper.filsToPageOfFils(fils);
        this.setSupprimable(auth, pageOfFils);
        this.setEditable(auth, pageOfFils);
        return pageOfFils;
    }

    /**
     * Gets the fortune of the day.
     *
     * @return the fortune of the day
     * @throws TechniqueException the technique exception
     * @throws FilServiceException the fil service exception
     */
    public FortuneDto getFortuneOfTheDay() throws TechniqueException, FilServiceException {
        LocalDate today = LocalDate.now();
        Fil fortuneOfTheDay = this.filRepository.findFortuneOfDay(LocalDateTime.of(today, LocalTime.of(0, 0, 0))).orElse(null);
        if (fortuneOfTheDay == null) {
            this.createFortuneOfTheDay();
            fortuneOfTheDay = this.filRepository.findFortuneOfDay(LocalDateTime.of(today, LocalTime.of(0, 0, 0))).orElse(null);
            if (fortuneOfTheDay == null) {
                throw new TechniqueException(IMPOSSIBLE_OBTENIR_FORTUNE_DU_JOUR);
            }
        }
        this.logger.info(fortuneOfTheDay.getMessage());
        return this.filMapper.filToFortuneDto(fortuneOfTheDay);
    }

    /**
     * Creates the fortune of the day.
     *
     * @return the fil
     * @throws TechniqueException the technique exception
     * @throws FilServiceException the fil service exception
     */
    private Fil createFortuneOfTheDay() throws TechniqueException, FilServiceException {
        
        FilToSaveDto fortuneDto = new FilToSaveDto();
        fortuneDto.setTitre("Citation du jour");
        fortuneDto.setMessage(this.systemService.getFortune());
        fortuneDto.setTags("fortune");
        
        Utilisateur system = this.utilisateurService.findByAuthenticationName(SYSTEM_AT_DUMMY_FR);
        this.enregistrerFil(system, fortuneDto);

        return null;
    }
    
    /**
     * Regchercher plus recent article par admin.
     *
     * @param tag the tag
     * @return the fil for page dto
     * @throws FilServiceException the fil service exception
     */
    public FilForPageDto regchercherPlusRecentArticleParAdmin(String tag) throws FilServiceException {
        Page<FilForPageDto> fil = this.rechercherDernierFil(this.forgePageRequestForlast(), tag);
        if (fil.getSize() != 1) {
            throw new FilServiceException(AUCUN_EDITO_TROUVE);
        }
        return fil.getContent().get(0);        
    }

    /**
     * Forge page request forlast.
     *
     * @return the page request dto
     */
    private PageRequestDto forgePageRequestForlast() {
        PageRequestDto pageRequestDto = new PageRequestDto();
        pageRequestDto.setSize(1);
        pageRequestDto.setNumber(0);
        return pageRequestDto;
    }
    
    /**
     * Rechercher dernier fil.
     *
     * @param pageResquestDto the page resquest dto
     * @param tag the tag
     * @return the page
     */
    public Page<FilForPageDto> rechercherDernierFil(PageRequestDto pageResquestDto, String tag) {
        Pageable pageRequest = this.paginationService.forgePageRequest(pageResquestDto, Fil.class);
        RubriqueSpecification spec = new RubriqueSpecification(tag, ProfilEnum.ADMIN);
        Page<Fil> fils = this.filRepository.findAll(spec, pageRequest);
        if (fils.isEmpty()) {
            return Page.empty();
        }
        return this.filMapper.filsToPageOfFils(fils);
    }

    /**
     * Contient image.
     *
     * @param imageDuMoment the image du moment
     * @throws FilServiceException the fil service exception
     */
    public void contientImage(FilForPageDto imageDuMoment) throws FilServiceException {
        if (imageDuMoment.getImage() == null || imageDuMoment.getImage().isBlank() || imageDuMoment.getImage().isEmpty()) {
            throw new FilServiceException("Pas d'image dans cet article.");
        }        
    }
}