package fr.nico.myawesomewebsite.metier;

import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;
import org.springframework.stereotype.Service;

@Service
public class SimpleMarkDownService {

    /**
     * Markdown to HTML.
     *
     * @param markdown the markdown
     * @return the string
     */
    public String markdownToHTML(String markdown) {
        Parser parser = Parser.builder().build();
        Node document = parser.parse(markdown);
        HtmlRenderer renderer = HtmlRenderer.builder().build();
        String html = renderer.render(document);

        return html.replace("<img", "<img class=\"in-article\"");
    }
}