package fr.nico.myawesomewebsite.metier;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.regex.Pattern;

import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import fr.nico.myawesomewebsite.domaine.entities.Fil;
import fr.nico.myawesomewebsite.domaine.entities.Utilisateur;
import fr.nico.myawesomewebsite.domaine.exception.TechniqueException;
import fr.nico.myawesomewebsite.webapp.dto.UtilisateurDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilToSaveDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilToUpdateDto;

/**
 * The Class FichierService.
 */
@Service
public class FichierService {

    /** The logger. */
    private Logger logger = LoggerFactory.getLogger(FichierService.class);

    /** The file upload directory. */
    @Value("${file.upload.directory}")
    private String fileUploadDirectory;

    /** The file upload resource name. */
    @Value("${file.upload.resource.name}")
    private String fileUploadResourceName;

    /**
     * Save image file.
     *
     * @param auteur the auteur
     * @param filToSaveDto the fil to save dto
     * @return the string
     */
    public String saveImageFile(Utilisateur auteur, FilToSaveDto filToSaveDto) {
        String path = "";
        try {
            path = this.doSaveFileOnDisk(auteur, filToSaveDto);
        } catch (IllegalStateException | IOException e) {
            this.logger.error(e.getMessage());
        }
        return path;
    }

    /**
     * Do save file on disk.
     *
     * @param auteur the auteur
     * @param filToSaveDto the fil to save dto
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private String doSaveFileOnDisk(Utilisateur auteur, FilToSaveDto filToSaveDto) throws IOException {
        String userUploadDirectory = this.getUserUploadDirectory(auteur);
        String yearMonthPath = this.getYearMonthPath(LocalDateTime.now());
        String fileName = this.getUniqueFileName();
        try {
            this.createSubDirectories(userUploadDirectory + File.separator + yearMonthPath);
        } catch (TechniqueException e) {
            this.logger.error(e.getMessage());
        }
        File file = new File(userUploadDirectory + File.separator + yearMonthPath + File.separator + fileName);
        filToSaveDto.getImage().transferTo(file);
        return fileName;
    }

    /**
     * Gets the unique file name.
     *
     * @return the unique file name
     */
    private String getUniqueFileName() {
        return UUID.randomUUID().toString();
    }

    /**
     * Gets the user upload directory.
     *
     * @param auteur the auteur
     * @return the user upload directory
     */
    public String getUserUploadDirectory(Utilisateur auteur) {
        File uploadDirectoryForUser = new File(this.fileUploadDirectory + File.separator + auteur.getId());
        if (!uploadDirectoryForUser.exists()) {
            uploadDirectoryForUser.mkdirs();
        }
        return uploadDirectoryForUser.toString();
    }

    /**
     * Gets the year month path.
     *
     * @param date the date
     * @return the year month path
     */
    private String getYearMonthPath(LocalDateTime date) {
        return this.getYearPath(date) + File.separator + this.getMonthPath(date);
    }

    /**
     * Gets the year path.
     *
     * @param date the date
     * @return the year path
     */
    private String getYearPath(LocalDateTime date) {
        return date.getYear() + "";
    }

    /**
     * Gets the month path.
     *
     * @param date the date
     * @return the month path
     */
    private String getMonthPath(LocalDateTime date) {
        return date.getMonthValue() + "";
    }

    /**
     * Creates the sub directories.
     *
     * @param path the path
     * @throws TechniqueException the technique exception
     */
    public void createSubDirectories(String path) throws TechniqueException {
        try {
            String[] arrayOfSubDirectories = this.getArrayOfDirectories(path);
            String subDirectory = "";
            int index = 0;
            while (index < arrayOfSubDirectories.length) {
                subDirectory = subDirectory.concat(arrayOfSubDirectories[index++] + File.separator);
                File directoryToCreate = new File(subDirectory);
                if (!directoryToCreate.exists()) {
                    directoryToCreate.mkdir();
                }
            }
        } catch (IllegalArgumentException e) {
            throw new TechniqueException("Impossible de créer les réperoires!");
        }
    }

    /**
     * Gets the array of directories.
     *
     * @param path the path
     * @return the array of directories
     * @throws IllegalArgumentException the illegal argument exception
     */
    private String[] getArrayOfDirectories(String path) throws IllegalArgumentException {
        if (this.isEmptyString(path)) {
            throw new IllegalArgumentException("Aucun chemin de répertoirs transmis!");
        }
        // https://stackoverflow.com/questions/10336293/splitting-filenames-using-system-file-separator-symbol
        String splitPattern = Pattern.quote(File.separator);
        return path.split(splitPattern);
    }

    /**
     * Checks if is empty string.
     *
     * @param path the path
     * @return true, if is empty string
     */
    private boolean isEmptyString(String path) {
        return (path == null) || path.isEmpty() || path.isBlank();
    }

    /**
     * Sets the full path to image.
     *
     * @param utilisateurDto the utilisateur dto
     * @param nomImage the nom image
     * @return the string
     */
    public String setFullPathToImage(UtilisateurDto utilisateurDto, String nomImage) {
        return this.fileUploadResourceName
                + File.separator
                + utilisateurDto.getId()
                + File.separator + nomImage;
    }

    /**
     * Update image file.
     *
     * @param auteur the auteur
     * @param filToUpdateDto the fil to update dto
     * @param filToUpdate the fil to update
     * @return the string
     */
    public String updateImageFile(Utilisateur auteur, FilToUpdateDto filToUpdateDto, Fil filToUpdate)
    {
        String userUploadDirectory = this.getUserUploadDirectory(auteur);
        String yearMonthPath = this.getYearMonthPath(filToUpdate.getDateCreation());
        String fileName = this.getUniqueFileName();
        try {
            this.createSubDirectories(userUploadDirectory + File.separator + yearMonthPath);
        } catch (TechniqueException e) {
            this.logger.error(e.getMessage());
        }
        File file = new File(userUploadDirectory + File.separator + yearMonthPath + File.separator + fileName);
        try {
            filToUpdateDto.getImage().transferTo(file);
        } catch (IllegalStateException | IOException e) {
            this.logger.error(e.getMessage());
        }
        return fileName;
    }

    /**
     * Delete image.
     *
     * @param filToUpdate the fil to update
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void deleteImage(Fil filToUpdate) throws IOException {

        String rootFolder = this.fileUploadDirectory + filToUpdate.getAuteur().getId();
        String yearFolder = rootFolder + File.separator + this.getYearPath(filToUpdate.getDateCreation());
        String monthFolder = yearFolder + File.separator + this.getMonthPath(filToUpdate.getDateCreation());

        Files.delete(Path.of(monthFolder + File.separator + filToUpdate.getNomImage()));

        this.cleanFoldersTree(rootFolder, yearFolder, monthFolder);
    }

    /**
     * Clean folders tree.
     *
     * @param rootFolder the root folder
     * @param yearFolder the year folder
     * @param monthFolder the month folder
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void cleanFoldersTree(String rootFolder, String yearFolder, String monthFolder) throws IOException {
        this.deleteFolderIfEmpty(monthFolder);
        this.deleteFolderIfEmpty(yearFolder);
        this.deleteFolderIfEmpty(rootFolder);
    }

    /**
     * Delete folder if empty.
     *
     * @param monthFolder the month folder
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void deleteFolderIfEmpty(String monthFolder) throws IOException {
        if (Files.list(Path.of(monthFolder)).count() == 0) {
            Files.delete(Path.of(monthFolder));
        }
    }

    /**
     * Check file media type.
     *
     * @param filToSaveDto the fil to save dto
     * @param mediaType the media type
     * @return true, if successful
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public boolean checkFileMediaType(FilToSaveDto filToSaveDto, String mediaType) throws IOException {
        String uploadedFileMediaType = this.detectDocType(filToSaveDto.getImage().getInputStream());
        return uploadedFileMediaType.split("/")[0].equals(mediaType);
    }

    /**
     * Detect doc type.
     *
     * @param stream the stream
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private String detectDocType(InputStream stream) throws IOException {
        Tika tika = new Tika();
        return tika.detect(stream);
    }
}
