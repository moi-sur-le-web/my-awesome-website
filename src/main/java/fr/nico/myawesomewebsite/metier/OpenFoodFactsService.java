package fr.nico.myawesomewebsite.metier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fr.nico.myawesomewebsite.domaine.entities.Ingredient;
import fr.nico.myawesomewebsite.domaine.exception.OpenFoodFactsServiceException;
import fr.nico.myawesomewebsite.domaine.exception.OpenFoodFactsServiceException.OpenFoodFactsServiceExceptionEnum;
import fr.nico.myawesomewebsite.persistance.IngredientRepository;
import fr.nico.myawesomewebsite.webapp.dto.IngredientDto;
import fr.nico.myawesomewebsite.webapp.dto.openfoodfacts.OpenFoodFactsResponseDto;
import fr.nico.myawesomewebsite.webapp.mapping.OpenFoodFactsMapper;

/**
 * The Class OpenFoodFactsService.
 */
@Service
public class OpenFoodFactsService {
	
	/** The logger. */
	private Logger logger = LoggerFactory.getLogger(OpenFoodFactsService.class);

	/** The Constant URL. */
	private static final String URL = "https://fr.openfoodfacts.org/api/v0/product/";

	/** The Constant URL_END. */
	private static final String URL_END = ".json";

	/** The Constant TAILLE_8_CODE_BARRE. */
	private static final int TAILLE_8_CODE_BARRE = 8;

	/** The Constant TAILLE_12_CODE_BARRE. */
	private static final int TAILLE_12_CODE_BARRE = 12;

	/** The Constant TAILLE_13_CODE_BARRE. */
	private static final int TAILLE_13_CODE_BARRE = 13;

	/** The Constant TAILLE_14_CODE_BARRE. */
	private static final int TAILLE_14_CODE_BARRE = 14;

	/** The Constant ZERO. */
	private static final String ZERO = "0";

	/** The open food facts mapper. */
	@Autowired
	private OpenFoodFactsMapper openFoodFactsMapper;

	/** The open food facts repository. */
	@Autowired
	private IngredientRepository ingredientRepository;
	
	/** The rest template. */
	@Autowired
	private RestTemplate restTemplate;
	
	/** The http agent. */
	@Value(value = "open.food.facts.user.agent")
	private String userAgent;

	/**
	 * Valider et formater code barre.
	 *
	 * @param codeBarre the code barre
	 * @return the string
	 * @throws OpenFoodFactsServiceException the open food facts service exception
	 */
	private String validerEtFormaterCodeBarre(String codeBarre) throws OpenFoodFactsServiceException {

		int tailleCodeBarre = codeBarre.length();

		// test taille du code barre
		if (tailleCodeBarre != TAILLE_8_CODE_BARRE && tailleCodeBarre != TAILLE_12_CODE_BARRE
				&& tailleCodeBarre != TAILLE_13_CODE_BARRE && tailleCodeBarre != TAILLE_14_CODE_BARRE) {

			// log
			logger.error(OpenFoodFactsServiceExceptionEnum.FORMAT_CODE_BARRE_INVALIDE.getMessage() 
					+ " [" + codeBarre.toString() + "]");
			
			// exception
			throw new OpenFoodFactsServiceException(OpenFoodFactsServiceExceptionEnum.FORMAT_CODE_BARRE_INVALIDE);
		}

		// Ajout de zéros à gauche pour avoir 14 caractères
		String zeros = "";
		for (int k = tailleCodeBarre; k < TAILLE_14_CODE_BARRE; ++k) {
			zeros = zeros.concat(ZERO);
		}
		codeBarre = zeros.concat(codeBarre);

		// calculs des sommes
		int somme = 0;
		for (int k = TAILLE_14_CODE_BARRE - 2; k > 0; k -= 2) {
			somme += codeBarre.charAt(k) - '0';
		}
		somme *= 3;
		for (int k = TAILLE_14_CODE_BARRE - 3; k >= 0; k -= 2) {
			somme += codeBarre.charAt(k) - '0';
		}

		// vérification avec la clé
		if ((somme + codeBarre.charAt(13) - '0') % 10 != 0) {

			// log
			logger.error(OpenFoodFactsServiceExceptionEnum.FORMAT_CODE_BARRE_INVALIDE.getMessage() 
					+ " [" + codeBarre.toString() + "]");
			
			// exception
			throw new OpenFoodFactsServiceException(OpenFoodFactsServiceExceptionEnum.FORMAT_CODE_BARRE_INVALIDE);
		}

		// fin si code barre OK
		return codeBarre;
	}
	
	/**
	 * Gets the header.
	 *
	 * @return the header
	 */
	private HttpHeaders getHeaders() {
		
		HttpHeaders headers = new HttpHeaders();
		
		headers.set(HttpHeaders.USER_AGENT, userAgent);
		headers.set(HttpHeaders.ACCEPT_CHARSET, "utf-8");
		
		return headers;
	}
	
	/**
	 * Gets the request.
	 *
	 * @return the request
	 */
	private HttpEntity<String> getRequest(){
		
		return new HttpEntity<>(this.getHeaders());
	}

	/**
	 * Rechercher valeurs nutritionnelles open food facts.
	 *
	 * @param codeBarre the code barre
	 * @return the ingredient dto
	 * @throws OpenFoodFactsServiceException the open food facts service exception
	 */
	public IngredientDto rechercherValeursNutritionnellesOpenFoodFacts(String codeBarre) throws OpenFoodFactsServiceException {
		
		// validation et formatage du code barre OU exception
		codeBarre = this.validerEtFormaterCodeBarre(codeBarre);
		
		// on vérifie que l'item n'est pas déjà présent dans la table des ingrédients
		if (this.ingredientRepository.existsByCodeBarre(codeBarre)) {
			throw new OpenFoodFactsServiceException(OpenFoodFactsServiceExceptionEnum.ITEM_EXISTANT);
		}
		
		// envoi de la requête
		String url = URL + codeBarre + URL_END;
		ResponseEntity<OpenFoodFactsResponseDto> openFoodFactsResponseDto = this.restTemplate.exchange(url, HttpMethod.GET, this.getRequest(), OpenFoodFactsResponseDto.class);

		// Status réponse 200 ?
		if (openFoodFactsResponseDto.getStatusCode() != HttpStatus.OK) {
			
			// log
			logger.error(OpenFoodFactsServiceExceptionEnum.ERREUR_SERVEUR.getMessage() 
					+ " [" + openFoodFactsResponseDto.getStatusCode() + "]");
			
			// exception
			throw new OpenFoodFactsServiceException(OpenFoodFactsServiceExceptionEnum.ERREUR_SERVEUR);
		}
		
		// vérification du corps de la réponse
		if (openFoodFactsResponseDto.getBody().getProductDto() == null 
				|| openFoodFactsResponseDto.getBody().getProductDto().getProduct_name_fr() == null
				|| openFoodFactsResponseDto.getBody().getProductDto().getNutriments() == null) {
			
			// log
			logger.error(OpenFoodFactsServiceExceptionEnum.PRODUIT_INCONNU.getMessage() 
					+ " [" + codeBarre + "]");
			
			// exception
			throw new OpenFoodFactsServiceException(OpenFoodFactsServiceExceptionEnum.PRODUIT_INCONNU);
		}
		
		// fin si réponse OK
		return this.openFoodFactsMapper.openFoodFactsResponseDtoToAlimentDto(openFoodFactsResponseDto.getBody(), codeBarre);
	}

	/**
	 * Enregistrer.
	 *
	 * @param ingredientDto the ingredient dto
	 * @return the ingredient dto
	 */
	public IngredientDto enregistrer(IngredientDto ingredientDto) {

		Ingredient ingredientToSave = this.openFoodFactsMapper.ingredientDtoToIngredient(ingredientDto);
		ingredientToSave = this.ingredientRepository.save(ingredientToSave);
		return this.openFoodFactsMapper.ingredientToIngredientDto(ingredientToSave);
	}
}
