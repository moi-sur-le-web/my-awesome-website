package fr.nico.myawesomewebsite.metier;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.nico.myawesomewebsite.domaine.entities.Metabolisme;
import fr.nico.myawesomewebsite.domaine.exception.RegleGestionException;
import fr.nico.myawesomewebsite.domaine.referentiel.TypeMetabolismeEnum;
import fr.nico.myawesomewebsite.persistance.MetabolismeRepository;
import fr.nico.myawesomewebsite.webapp.dto.GabaritDto;
import fr.nico.myawesomewebsite.webapp.dto.MetabolismeDto;
import fr.nico.myawesomewebsite.webapp.dto.RythmeDto;
import fr.nico.myawesomewebsite.webapp.dto.UtilisateurDto;
import fr.nico.myawesomewebsite.webapp.mapping.MetabolismeMapper;

/**
 * The Class MetabolismeService.
 */
@Service
@Transactional
public class MetabolismeService {

    /** The metabolisme repository. */
    @Autowired
    private MetabolismeRepository metabolismeRepository;
    
    /** The metabolisme mapper. */
    @Autowired
    private MetabolismeMapper metabolismeMapper;
    
    /** The gabarit service. */
    @Autowired
    private GabaritService gabaritService;

    /**
     * Calculer basal.
     *
     * @param anneeDeNaissance the annee de naissance
     * @param taille the taille
     * @param poids the poids
     * @return la valeur du métabolisme basal
     */
    private int calculerBasal(int anneeDeNaissance, int taille, double poids) {

        int age = Calendar.getInstance().get(Calendar.YEAR) - anneeDeNaissance;
        return ( int )( 1.083 * Math.pow(poids, 0.48) * Math.pow(taille / 100., 0.5) * Math.pow(age, -0.13) * (1000./4.1855) );        
    }
    
    /**
     * Calculer maintient.
     *
     * @param basal the basal
     * @param rythme the rythme
     * @return the int
     */
    private int calculerMaintient(int basal, RythmeDto rythme) {
    	
    	return (int) (basal * rythme.getEntrainementHebdomadaire().getCoefficient());
    }
    
    /**
     * Calculer seche.
     *
     * @param maintient the maintient
     * @return the int
     */
    private int calculerSeche(int maintient) {
    	
    	return (int) (0.9 * maintient);
    }

    /**
     * Calculer masse.
     *
     * @param maintient the maintient
     * @return the int
     */
    private int calculerMasse(int maintient) {
    	
    	return (int) (1.1 * maintient);
    }
    
    /**
     * Calculer proteines.
     *
     * @param gabaritDto the gabarit dto
     * @param rythmeDto the rythme dto
     * @return the int
     */
    private int calculerProteines(GabaritDto gabaritDto, RythmeDto rythmeDto) {
    	
    	return (int) (gabaritDto.getPoids() * rythmeDto.getProteineKilo() / rythmeDto.getNbRepasJour());
    }

    /**
     * Calculer lipides.
     *
     * @param gabaritDto the gabarit dto
     * @param rythmeDto the rythme dto
     * @return the int
     */
    private int calculerLipides(GabaritDto gabaritDto, RythmeDto rythmeDto) {
    	
    	return (int) (gabaritDto.getPoids() * rythmeDto.getLipideKilo() / rythmeDto.getNbRepasJour());
    }
    
    /**
     * Calculer glucides.
     *
     * @param proteines the proteines
     * @param lipides the lipides
     * @param maintient the metabolisme dto
     * @param rythmeDto the rythme dto
     * @return the int
     */
    private int calculerGlucides(int proteines, int lipides, int metabolisme, RythmeDto rythmeDto) {
    	
    	return (int) (metabolisme / rythmeDto.getNbRepasJour() - (4 * proteines + 9 * lipides)) / 4;
    }

    /**
     * Initialiser tout.
     *
     * @param utilisateurDto the utilisateur dto
     * @param gabaritDtoParDefaut the gabarit dto par defaut
     * @return the list
     */
    public List<MetabolismeDto> initialiserBasal(UtilisateurDto utilisateurDto, GabaritDto gabaritDtoParDefaut) {
    	
    	List<Metabolisme> metabolismesToSave = new ArrayList<>();
    	
    	// calcul des nouvelles valeurs pour chaque métabolisme
    	int basal = this.calculerBasal(
    			utilisateurDto.getAnneeDeNaissance(),
    			gabaritDtoParDefaut.getTaille(),
    			gabaritDtoParDefaut.getPoids());
    	
    	// affectation des nouvelle valeurs
    	metabolismesToSave.add(new Metabolisme(basal, TypeMetabolismeEnum.BASAL, 0.0, 0.0, 0.0));
    	
    	// persistance
    	metabolismesToSave = this.metabolismeRepository.saveAll(metabolismesToSave);
    	
    	// fin
    	return this.metabolismeMapper.metabolismeToMetabolismeDto(metabolismesToSave);
    }
    
    /**
     * Initialiser maintient seche masse.
     *
     * @param utilisateurDto the utilisateur dto
     * @param rythmeDto the rythme dto
     * @return the list
     */
    public List<MetabolismeDto> initialiserMaintientSecheMasse(UtilisateurDto utilisateurDto, RythmeDto rythmeDto) {
    	
    	List<Metabolisme> metabolismes = this.metabolismeRepository.getMetabolismesByUserId(utilisateurDto.getId()).orElse(null);
    	if (metabolismes == null) {
    		throw new RegleGestionException("Aucun métabolisme n'existe en base de données pour cette utilisateur!");
    	}
    	
    	// récupération du métabolisme basal
    	int basal = metabolismes.get(0).getValeur();
    	
    	// calcul des métabolismes de maintient, de sèche et de prise de masse
    	int maintient = this.calculerMaintient(basal, rythmeDto);
    	int seche = this.calculerSeche(maintient);
    	int masse = this.calculerMasse(maintient);
    	
    	// calculs des quantités de protéines, lipides et glucides par repas et par type de métabolisme
    	GabaritDto gabaritDto = this.gabaritService.rechercherParUtilisateur(utilisateurDto);
    	if (gabaritDto == null) {
    		throw new RegleGestionException("Aucun gabarit n'existe en base de données pour cette utilisateur!");
    	}
    	int proteines = this.calculerProteines(gabaritDto, rythmeDto);
    	int lipides = this.calculerLipides(gabaritDto, rythmeDto);
    	int glucidesMaintient = this.calculerGlucides(proteines, lipides, maintient, rythmeDto);
    	int glucidesSeche = this.calculerGlucides(proteines, lipides, seche, rythmeDto);
    	int glucidesMasse = this.calculerGlucides(proteines, lipides, masse, rythmeDto);
    	
    	// affectation des métabolismes calculés à la liste des métabolismes
    	metabolismes.add(new Metabolisme(maintient, TypeMetabolismeEnum.MAINTIENT, proteines, lipides, glucidesMaintient));
    	metabolismes.add(new Metabolisme(seche, TypeMetabolismeEnum.SECHE, proteines, lipides, glucidesSeche));
    	metabolismes.add(new Metabolisme(masse, TypeMetabolismeEnum.MASSE, proteines, lipides, glucidesMasse));
    	
    	// persistance
    	metabolismes = this.metabolismeRepository.saveAll(metabolismes);
    	
    	//fin    	
    	return this.metabolismeMapper.metabolismeToMetabolismeDto(metabolismes);
    }

    /**
     * Actualiser tous les métabolismes.
     *
     * @param utilisateurDto the utilisateur dto
     * @param gabaritDto the gabarit dto
     * @param rythmeDto the rythme dto
     * @return the list
     */
    public List<MetabolismeDto> actualiserTout(UtilisateurDto utilisateurDto, GabaritDto gabaritDto, RythmeDto rythmeDto) {
        
        // recherche des métabolismes
        List<Metabolisme> metabolismesBdd = this.metabolismeRepository
                .getMetabolismesByUserId(utilisateurDto.getId())
                .orElse(null);
        
        if (metabolismesBdd == null) {
            throw new RegleGestionException("Aucun métabolisme enregistré en base de données pour cet utilisateur");
        }
        
        // calcul des nouvelles valeurs pour chaque métabolisme
        // les calculs des métabolismes de sèche, de maintient et de prise de masse
        // sont possibles uniquement si l'utilisateur a initialisé un rythme.
        int basal = this.calculerBasal(utilisateurDto.getAnneeDeNaissance(), gabaritDto.getTaille(), gabaritDto.getPoids());        
        int maintient = 0; 
        int seche = 0;
        int masse = 0;
        int proteines = 0;
    	int lipides = 0;
    	int glucidesMaintient = 0;
    	int glucidesSeche = 0;
    	int glucidesMasse = 0;
        if (rythmeDto != null) {
        	maintient = this.calculerMaintient(basal, rythmeDto);
        	seche = this.calculerSeche(maintient);
        	masse = this.calculerMasse(maintient);
        	proteines = this.calculerProteines(gabaritDto, rythmeDto);
        	lipides = this.calculerLipides(gabaritDto, rythmeDto);
        	glucidesMaintient = this.calculerGlucides(proteines, lipides, maintient, rythmeDto);
        	glucidesSeche = this.calculerGlucides(proteines, lipides, seche, rythmeDto);
        	glucidesMasse = this.calculerGlucides(proteines, lipides, masse, rythmeDto);
        }
        
        // affectation des nouvelles valeurs
        for (Metabolisme met : metabolismesBdd) {
            
            switch (met.getType()) {                
                case BASAL:
                    met.setValeur(basal);
                    break;
                case MAINTIENT:
                	met.setValeur(maintient);
                	met.setProteinesRepas(proteines);
                	met.setLipidesRepas(lipides);
                	met.setGlucidesRepas(glucidesMaintient);
                	break;
                case SECHE:
                	met.setValeur(seche);
                	met.setProteinesRepas(proteines);
                	met.setLipidesRepas(lipides);
                	met.setGlucidesRepas(glucidesSeche);
                	break;
                case MASSE:
                	met.setValeur(masse);
                	met.setProteinesRepas(proteines);
                	met.setLipidesRepas(lipides);
                	met.setGlucidesRepas(glucidesMasse);
                	break;
                default :
                    break;                    
            }            
        }
        
        // persistance
        metabolismesBdd = this.metabolismeRepository.saveAll(metabolismesBdd);
        
        // fin
        return this.metabolismeMapper.metabolismeToMetabolismeDto(metabolismesBdd);        
    }
}
