package fr.nico.myawesomewebsite.metier;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import fr.nico.myawesomewebsite.domaine.entities.Fil;
import fr.nico.myawesomewebsite.webapp.dto.PageRequestDto;

/**
 * The Class PaginationService.
 */
@Service
public class PaginationService {

    /** The logger. */
    private Logger logger = LoggerFactory.getLogger(PaginationService.class);

    /** The Constant DOUBLE_DOTS_DELIMITER. */
    private static final String SPLIT_SEPARATOR = ":";

    /** The technique service. */
    @Autowired
    private TechniqueService techniqueService;

    /** The default page index. */
    @Value("${pagination.default.index}")
    private int defaultPageIndex;

    /** The default page size. */
    @Value("${pagination.default.page.size}")
    private int defaultPageSize;

    /** The default sort column. */
    @Value("${pagination.default.sort.column}")
    private String defaultSortColumn;

    /**
     * Forge page request.
     *
     * @param pageResquestDto the page resquest dto
     * @param classToParseForSortFields the class to parse for sort fields
     * @return the page request
     */
    public PageRequest forgePageRequest(PageRequestDto pageResquestDto, Class<?> classToParseForSortFields) {

        int pageIndex = this.defaultPageIndex;
        int size = this.defaultPageSize;
        if (this.isValidPageIndexAndSize(pageResquestDto)) {
            pageIndex = pageResquestDto.getNumber();
            size = pageResquestDto.getSize();
        }
        List<Order> orders = this.getOrders(pageResquestDto, classToParseForSortFields);
        return PageRequest.of(pageIndex, size, Sort.by(orders));
    }

    /**
     * Checks if is valid page index and size.
     *
     * @param pageResquestDto the page resquest dto
     * @return true, if is valid page index and size
     */
    private boolean isValidPageIndexAndSize(PageRequestDto pageResquestDto) {
        return this.isValidPageIndex(pageResquestDto) && this.isValidPageSize(pageResquestDto);
    }

    /**
     * Checks if is valid page size.
     *
     * @param pageResquestDto the page resquest dto
     * @return true, if is valid page size
     */
    private boolean isValidPageSize(PageRequestDto pageResquestDto) {
        return (pageResquestDto.getSize() != null) && (pageResquestDto.getSize() >= 1);
    }

    /**
     * Checks if is valid page index.
     *
     * @param pageResquestDto the page resquest dto
     * @return true, if is valid page index
     */
    private boolean isValidPageIndex(PageRequestDto pageResquestDto) {
        return (pageResquestDto.getNumber() != null) && (pageResquestDto.getNumber() >= 0);
    }

    /**
     * Gets the orders.
     *
     * @param pageResquestDto the page resquest dto
     * @param classToParseForSortFields the class to parse for sort fields
     * @return the orders
     */
    public List<Order> getOrders(PageRequestDto pageResquestDto, Class<?> classToParseForSortFields) {
        List<Order> ordersToReturn = new ArrayList<>();
        ordersToReturn.add(new Order(Sort.Direction.DESC, this.defaultSortColumn));
        if (!pageResquestDto.getSortColsAndDirections().isEmpty()) {
            List<Order> userOrders = this.forgeOrdersList(pageResquestDto.getSortColsAndDirections());
            if (this.checkOrdersColsNamesAndClassFieldNames(userOrders, this.techniqueService.getFieldsList(classToParseForSortFields))) {
                ordersToReturn = userOrders;
            }
        }
        this.logger.info("Ouverture du fil - ordre de tri : {}", ordersToReturn);
        return ordersToReturn;
    }

    /**
     * Forge orders list.
     *
     * @param sortColsAndDirections the sort cols and directions
     * @return the list
     */
    private List<Order> forgeOrdersList(List<String> sortColsAndDirections) {
        List<Order> orders = new ArrayList<>();
        boolean ok = true;
        int k = 0;
        while (ok && (k < sortColsAndDirections.size())) {
            String[] colAndDirSplited = StringUtils.split(sortColsAndDirections.get(k), SPLIT_SEPARATOR);
            if ((colAndDirSplited != null) && (colAndDirSplited[1].equalsIgnoreCase("asc") || colAndDirSplited[1].equalsIgnoreCase("desc"))) {
                orders.add(new Order(Sort.Direction.fromString(colAndDirSplited[1]), colAndDirSplited[0]));
            } else {
                ok = false;
            }
            ++k;
        }
        return ok ? orders : new ArrayList<>();
    }

    /**
     * Check orders cols names and class field names.
     *
     * @param orders the orders
     * @param fieldNames the field names
     * @return true, if successful
     */
    private boolean checkOrdersColsNamesAndClassFieldNames(List<Order> orders, List<String> fieldNames) {
        boolean propertiesFound = false;
        for (Order o : orders) {
            if (fieldNames.contains(o.getProperty())) {
                propertiesFound = true;
                this.logger.info("Colonne de tri {} trouvée dans classe {}.", o.getProperty(), Fil.class.getName());
            } else {
                propertiesFound = false;
                this.logger.info("Colonne de tri {} absente de la classe {}.", o.getProperty(), Fil.class.getCanonicalName());
            }
        }
        return propertiesFound;
    }

}
