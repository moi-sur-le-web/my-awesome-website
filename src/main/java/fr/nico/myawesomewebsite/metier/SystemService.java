package fr.nico.myawesomewebsite.metier;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.stereotype.Service;

import fr.nico.myawesomewebsite.domaine.exception.TechniqueException;

/**
 * The Class SystemService.
 */
@Service
public class SystemService {
    
  
    /**
     * Gets the fortune.
     *
     * @return the fortune
     * @throws TechniqueException the technique exception
     */
    public String getFortune() throws TechniqueException {
        String output = "";
        try {
            output = this.cli("/bin/bash", "-c", "/usr/games/fortune");
        } catch (IOException | InterruptedException e) {
           throw new TechniqueException("Impossible d'obtenir une fortune.");
        }
        return output;
    }

    /**
     * Cli.
     *
     * @param cli the cli
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws InterruptedException the interrupted exception
     */
    private String cli(String... cli) throws IOException, InterruptedException {
        
        ProcessBuilder processBuilder = new ProcessBuilder().command(cli);
        StringBuilder output = new StringBuilder();    
        Process process = processBuilder.start();
        InputStreamReader inputStreamReader = new InputStreamReader(process.getInputStream());
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            output.append(line);
        }
        process.waitFor();
        bufferedReader.close();

        return output.toString();
    }
}
