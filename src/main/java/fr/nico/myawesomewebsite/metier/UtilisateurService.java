package fr.nico.myawesomewebsite.metier;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import fr.nico.myawesomewebsite.domaine.entities.Utilisateur;
import fr.nico.myawesomewebsite.domaine.exception.RegleGestionException;
import fr.nico.myawesomewebsite.domaine.exception.TechniqueException;
import fr.nico.myawesomewebsite.domaine.referentiel.ProfilEnum;
import fr.nico.myawesomewebsite.persistance.UtilisateurRepository;
import fr.nico.myawesomewebsite.webapp.dto.ProfilUtilisateurDto;
import fr.nico.myawesomewebsite.webapp.dto.UtilisateurDto;
import fr.nico.myawesomewebsite.webapp.mapping.UtilisateurMapper;

@Service
@Transactional
public class UtilisateurService {

    @Autowired
    private UtilisateurRepository utilisateurRepository;
    
    @Autowired
    private UtilisateurMapper utilisateurMapper;
    
    private static final String AUTHENTICATION_NULL = "Authentication null";
    
    private static final String AUCUN_UTILISATEUR_EN_BDD_AVEC_EMAIL = "Aucun utilisateur en base de données avec l'email ";
    
    /**
     * Attribuer role utilisateur.
     *
     * @param utilisateur the utilisateur DTO
     */
    private void attribuerRoleUtilisateur(Utilisateur utilisateur) {
        long nbUserInDb = this.utilisateurRepository.count();
        if (nbUserInDb == 0) {
            utilisateur.setProfil(ProfilEnum.ADMIN);
        } else {
            utilisateur.setProfil(ProfilEnum.USER);
        }
    }

    /**
     * Rechercher utilisateur avec gabarit.
     *
     * @param mail the mail
     * @return the utilisateur DTO
     */
    public UtilisateurDto rechercherUtilisateurToutesDonnees( String mail ) {

        Utilisateur utilisateur = this.utilisateurRepository.findByMailFetchAll(mail).orElse(null);
        if (utilisateur == null) {
            throw new RegleGestionException("Aucun utilisateur ayant l'adresse mail " + mail);
        }
        return this.utilisateurMapper.utilisateurToUtilisateurDto(utilisateur);
    }

    /**
     * Inscrire utilisateur.
     *
     * @param nouvelUtilisateurDto the utilisateur DTO
     * @return the utilisateur DTO
     */
    public void inscrireUtilisateur(ProfilUtilisateurDto nouvelUtilisateurDto) {

        nouvelUtilisateurDto.setMotDePasse(this.cryptageMotDePasse(nouvelUtilisateurDto.getMotDePasse()));

        Utilisateur utilisateur = this.utilisateurMapper.nouvelUtilisateurDtoToUtilisateur(nouvelUtilisateurDto);
        this.attribuerRoleUtilisateur(utilisateur);

        this.utilisateurRepository.save(utilisateur);
    }

    /**
     * @param motDePasseEnClair
     * @return
     */
    private String cryptageMotDePasse(String motDePasseEnClair) {
        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        return encoder.encode(motDePasseEnClair);
    }


    /**
     * Gets the liste utilisateurs.
     *
     * @return the liste utilisateurs
     */
    public List<UtilisateurDto> getListeUtilisateurs() {

        Iterable<Utilisateur> uListe = this.utilisateurRepository.findAll();
        return this.utilisateurMapper.utilisateurToUtilisateurDto(uListe);
    }

    /**
     * Enregistrer utilisateur.
     *
     * @param utilisateurDto the utilisateur dto
     * @return the utilisateur DTO
     */
    public UtilisateurDto enregistrerUtilisateur(UtilisateurDto utilisateurDto) {

        // utilisateur en bdd à mettre à jour
        Utilisateur utilisateurBdd = this.utilisateurRepository.findByMail(utilisateurDto.getMail()).orElse(null);
        if (utilisateurBdd == null) {
            throw new RegleGestionException(AUCUN_UTILISATEUR_EN_BDD_AVEC_EMAIL + utilisateurDto.getMail());
        }
        Utilisateur utilisateurToSave = this.utilisateurMapper.utilisateurDtoToUtilisateur(utilisateurDto);
        utilisateurToSave = this.utilisateurMapper.miseAJourUtilisateur(utilisateurToSave, utilisateurBdd);
        utilisateurToSave = this.utilisateurRepository.save(utilisateurToSave);

        // fin
        return this.utilisateurMapper.utilisateurToUtilisateurDto(utilisateurToSave);
    }

    /**
     * Rechercher utilisateur.
     *
     * @param mail the mail
     * @return the utilisateur DTO
     */
    public UtilisateurDto rechercherUtilisateur(String mail) {

        Utilisateur utilisateur = this.utilisateurRepository.findByMail(mail).orElse(null);
        if (utilisateur == null) {
            throw new RegleGestionException("Aucun utilisateur ayant l'adresse mail " + mail);
        }
        return this.utilisateurMapper.utilisateurToUtilisateurDto(utilisateur);
    }

    /**
     * Find by authentication name.
     *
     * @param name the name
     * @return the utilisateur
     */
    public Utilisateur findByAuthenticationName(String name) {
        return this.utilisateurRepository.findByMail(name).orElse(null);
    }

    /**
     * Valider nouvel utilisateur.
     *
     * @param nouveau the nouveau
     * @param bindingResult
     * @return the map
     */
    public List<String> validerNouvelUtilisateur(ProfilUtilisateurDto nouveau, BindingResult bindingResult) {

        List<String> errors = new ArrayList<>();
        this.checkPrenom(nouveau, errors);
        this.checkNom(nouveau, errors);
        this.checkSex(nouveau, errors);
        this.checkMail(nouveau, bindingResult, errors);
        this.checkPassword(nouveau.getMotDePasse(), bindingResult, errors);
        this.checkBirth(nouveau, errors);
        return errors;
    }
    
    /** 
     * Valider mise à jour profil utilisateur.
     * 
     * @param profil
     * @param bindingResult
     * @return List<String>
     * @throws TechniqueException
     */
    public List<String> validerMiseAJourProfilUtilisateur(Authentication auth, ProfilUtilisateurDto profil, BindingResult bindingResult) throws TechniqueException {
        if (auth == null || this.noString(auth.getName())){
            throw new TechniqueException(AUTHENTICATION_NULL);
        }
        List<String> errors = new ArrayList<>();
        this.checkPrenom(profil, errors);
        this.checkNom(profil, errors);
        this.checkSex(profil, errors);
        this.checkPassword(profil.getMotDePasse(), bindingResult, errors);
        this.checkBirth(profil, errors);
        if (!auth.getName().equals(profil.getMail())){
            this.checkMail(profil, bindingResult, errors);
        }
        return errors;
    }

    private void checkBirth(ProfilUtilisateurDto nouveau, List<String> errors) {
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        if ((nouveau.getAnneeDeNaissance() > thisYear)
                || ((thisYear - nouveau.getAnneeDeNaissance()) < 18)
                || ((thisYear - nouveau.getAnneeDeNaissance()) > 150)) {
            errors.add("anneeDeNaissance");
        }
    }

    private void checkPassword(String motDePasse, BindingResult bindingResult, List<String> errors) {
        if (this.noString(motDePasse)) {
            errors.add("motDePasseObligatoire");
        } else {
            if (bindingResult.hasFieldErrors("motDePasse")) {
                errors.add("formatMotDePasse");
            }
        }
    }

    private void checkMail(ProfilUtilisateurDto nouveau, BindingResult bindingResult, List<String> errors) {
        if (this.noString(nouveau.getMail())) {
            errors.add("mailObligatoire");
        } else {
            if (bindingResult.hasFieldErrors("mail")) {
                errors.add("formatAddresseMail");
            } else {
                if (this.utilisateurRepository.existsByMail(nouveau.getMail())) {
                    errors.add("emailExisteDeja");
                }
            }
        }
    }

    private void checkSex(ProfilUtilisateurDto nouveau, List<String> errors) {
        if (nouveau.getSexe() == null) {
            errors.add("sexeObligatoire");
        }
    }

    private void checkNom(ProfilUtilisateurDto nouveau, List<String> errors) {
        if (this.noString(nouveau.getNom())) {
            errors.add("nomObligatoire");
        } else {
            if (nouveau.getNom().length() < 2) {
                errors.add("nomTropCourt");
            }
        }
    }

    private void checkPrenom(ProfilUtilisateurDto nouveau, List<String> errors) {
        if (this.noString(nouveau.getPrenom())) {
            errors.add("prenomObligatoire");
        } else {
            if (nouveau.getPrenom().length() < 2) {
                errors.add("prenomTropCourt");
            }
        }
    }

    /**
     * No string.
     *
     * @param string the string
     * @return true, if successful
     */
    private boolean noString(String string) {
        return (string == null) || string.isBlank() || string.isEmpty();
    }

    
    /** 
     * Récupérer les informations de profil utilisateur
     * 
     * @param auth
     * @return ProfilUtilisateurDto
     * @throws TechniqueException
     */
    public ProfilUtilisateurDto getProfilUtilisateur(Authentication auth) throws TechniqueException {
        
        if (auth == null || auth.getName() == null) {
            throw new TechniqueException(AUTHENTICATION_NULL);
        }
        Utilisateur utilisateur = this.findByAuthenticationName(auth.getName());
        if (utilisateur == null) {
            throw new TechniqueException(AUCUN_UTILISATEUR_EN_BDD_AVEC_EMAIL + auth.getName());
        }
        return this.utilisateurMapper.utilisateurToProfilUtilisateurDto(utilisateur);
    }

    public UtilisateurDto updateProfilUtilisateur(Authentication auth, ProfilUtilisateurDto profilUtilisateurDto) throws TechniqueException {

        if (auth == null) {
            throw new TechniqueException(AUTHENTICATION_NULL);
        }        
        Utilisateur utilisateur = this.utilisateurRepository.findByMail(auth.getName()).orElse(null);
        if (utilisateur == null) {
            throw new TechniqueException(AUCUN_UTILISATEUR_EN_BDD_AVEC_EMAIL + auth.getName());
        }
        this.updateUtilisateurObject(profilUtilisateurDto, utilisateur);
        utilisateur = this.utilisateurRepository.save(utilisateur);
        return this.utilisateurMapper.utilisateurToUtilisateurDto(utilisateur);
    }

    private void updateUtilisateurObject(ProfilUtilisateurDto profilUtilisateurDto, Utilisateur utilisateur) {
        utilisateur.setNom(profilUtilisateurDto.getNom());
        utilisateur.setPrenom(profilUtilisateurDto.getPrenom());
        utilisateur.setSexe(profilUtilisateurDto.getSexe());
        utilisateur.setMail(profilUtilisateurDto.getMail());
        utilisateur.setMotDePasse(this.cryptageMotDePasse(profilUtilisateurDto.getMotDePasse()));
        utilisateur.setAnneeDeNaissance(profilUtilisateurDto.getAnneeDeNaissance());
    }
}
