package fr.nico.myawesomewebsite.metier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.nico.myawesomewebsite.domaine.entities.Fil;
import fr.nico.myawesomewebsite.domaine.entities.Tag;
import fr.nico.myawesomewebsite.persistance.TagRepository;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilToSaveDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilToUpdateDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.TagDto;
import fr.nico.myawesomewebsite.webapp.mapping.TagMapper;

/**
 * The Class TagService.
 */
@Service
public class TagService {

    /** The tag repository. */
    @Autowired
    private TagRepository tagRepository;
    
    /** The tag mapper. */
    @Autowired
    private TagMapper tagMapper;

    /**
     * Update tags.
     *
     * @param filToUpdateDto the fil to update dto
     * @param filToUpdate the fil to update
     */
    public void updateTags(FilToUpdateDto filToUpdateDto, Fil filToUpdate) {
        this.dropTags(filToUpdate);
        this.saveTags(filToUpdate, filToUpdateDto);
    }

    /**
     * Save tags.
     *
     * @param filToSave the fil to save
     * @param filToSaveDto the fil to save dto
     */
    public void saveTags(Fil filToSave, FilToSaveDto filToSaveDto) {
        List<Tag> tagsToSave = this.getTagsListToSave(filToSaveDto.getTags());
        if (!tagsToSave.isEmpty()) {
            this.tagRepository.saveAll(tagsToSave);
            filToSave.setTags(tagsToSave);
        }
    }

    /**
     * Drop tags.
     *
     * @param filToUpdate the fil to update
     */
    private void dropTags(Fil filToUpdate) {
        if (!filToUpdate.getTags().isEmpty()) {
            List<Tag> filTags = new ArrayList<>();
            for (Tag tag : filToUpdate.getTags()) {
                filTags.add(tag);
                tag.setNbAttributions(tag.getNbAttributions() - 1);
            }
            filToUpdate.getTags().removeAll(filTags);
        }
    }

    /**
     * Gets the tags list to save.
     *
     * @param stringOfTags the string of tags
     * @return the tags list to save
     */
    private List<Tag> getTagsListToSave(String stringOfTags) {
        String[] tagStringsArray = this.getArrayOfTagStrings(stringOfTags);
        if (tagStringsArray == null) {
            return Collections.emptyList();
        }
        List<Tag> tags = new ArrayList<>();
        for (String libelle : tagStringsArray) {
            Tag tag = this.tagRepository.findByLibelle(libelle).orElse(null);
            if (tag == null) {
                tag = this.createNewTag(libelle);
            } else {
                tag.setNbAttributions(tag.getNbAttributions() + 1);
            }
            if (!tags.contains(tag)) {
                tags.add(tag);
            }
        }
        return tags;
    }

    /**
     * Gets the array of tag strings.
     *
     * @param stringOfTags the string of tags
     * @return the array of tag strings
     */
    private String[] getArrayOfTagStrings(String stringOfTags) {
        if (this.isStringUsable(stringOfTags)) {
            return this.cleanStringOfTags(stringOfTags).split(",");
        }
        return new String[0];
    }

    /**
     * Checks if is string usable.
     *
     * @param stringOftags the string oftags
     * @return true, if is string usable
     */
    private boolean isStringUsable(String stringOftags) {
        return (stringOftags != null) && !stringOftags.isBlank() && !stringOftags.isEmpty();
    }

    /**
     * Clean string of tags.
     *
     * @param stringOftags the string oftags
     * @return the string
     */
    private String cleanStringOfTags(String stringOftags) {
        stringOftags = stringOftags.trim();
        stringOftags = stringOftags.replace(" ", "");
        stringOftags = stringOftags.toLowerCase();
        return stringOftags;
    }

    /**
     * Creates the new tag.
     *
     * @param libelle the libelle
     * @return the tag
     */
    private Tag createNewTag(String libelle) {
        Tag tag = new Tag();
        tag.setLibelle(libelle);
        tag.setNbAttributions(1);
        return tag;
    }
    
    /**
     * Mur tags.
     *
     * @return the list
     */
    public List<TagDto> murTags(){
        return this.tagMapper.tagToTagDto(this.tagRepository.findForMurTags());
    }
}