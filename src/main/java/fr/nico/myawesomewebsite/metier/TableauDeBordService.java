package fr.nico.myawesomewebsite.metier;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.nico.myawesomewebsite.webapp.dto.GabaritDto;
import fr.nico.myawesomewebsite.webapp.dto.MetabolismeDto;
import fr.nico.myawesomewebsite.webapp.dto.RythmeDto;
import fr.nico.myawesomewebsite.webapp.dto.UtilisateurDto;

/**
 * The Class TableauDeBordService.
 */
@Service
@Transactional
public class TableauDeBordService {

	/** The gabarit service. */
	@Autowired
	private GabaritService gabaritService;
	
	/** The rythme service. */
	@Autowired
	private RythmeService rythmeService;
	
	/** The metabolisme service. */
	@Autowired
	private MetabolismeService metabolismeService;
	
	/** The utilisateur service. */
	@Autowired
	private UtilisateurService utilisateurService;

    /**
     * Associer gabarit.
     *
     * @param utilisateurDto the utilisateur dto
     * @param gabaritDto the gabarit dto
     * @return the utilisateur DTO
     */
    private UtilisateurDto associerGabarit(UtilisateurDto utilisateurDto, GabaritDto gabaritDto) {
               
        utilisateurDto.setGabaritDto(gabaritDto);
        return utilisateurDto;
    }
    
    /**
     * Associer rythme.
     *
     * @param utilisateurDto the utilisateur dto
     * @param rythmeDto the rythme dto
     * @return the utilisateur DTO
     */
    private UtilisateurDto associerRythme(UtilisateurDto utilisateurDto, RythmeDto rythmeDto) {
    	
    	utilisateurDto.setRythmeDto(rythmeDto);
    	return utilisateurDto;
    }
    
    /**
     * Associer metabolismes.
     *
     * @param utilisateurDto the utilisateur dto
     * @param metabolismesDto the metabolismes dto
     * @return the utilisateur DTO
     */
    private UtilisateurDto associerMetabolismes(UtilisateurDto utilisateurDto, List<MetabolismeDto> metabolismesDto) {
              
        utilisateurDto.setMetabolismesDto(metabolismesDto);
        return utilisateurDto;        
    } 
    
    /**
     * Initialiser premier gabarit.
     *
     * @param utilisateurDto the utilisateur dto
     * @return the utilisateur DTO
     */
    public UtilisateurDto initialiserGabarit(UtilisateurDto utilisateurDto) {
        
        // initialisation du gabarit
        GabaritDto gabaritDtoParDefaut = this.gabaritService.donnerGabaritParDefaut(utilisateurDto);
        gabaritDtoParDefaut = this.gabaritService.enregistrerGabarit(gabaritDtoParDefaut);
        this.associerGabarit(utilisateurDto, gabaritDtoParDefaut);
        
        // initialisation du métabolisme basal
        List<MetabolismeDto> metabolismesDto = this.metabolismeService.initialiserBasal(utilisateurDto, gabaritDtoParDefaut);
        this.associerMetabolismes(utilisateurDto, metabolismesDto);
        
        // actualisation de l'utilisateur
        return this.utilisateurService.enregistrerUtilisateur(utilisateurDto);    
    }

    /**
     * Initialiser rythme.
     *
     * @param utilisateurDto the utilisateur dto
     * @return the utilisateur dto
     */
    public UtilisateurDto initialiserRythme(UtilisateurDto utilisateurDto) {
    	
    	// initialisation du rythme d'entrainement hebdomadaire et d'alimentation journalière
    	RythmeDto rythmeDto = this.rythmeService.donnerRythmeParDefaut();
    	this.associerRythme(utilisateurDto, rythmeDto);
    	
    	// initialisation des métabolismes de maintient, de sèche et de prise de masse
    	List<MetabolismeDto> metabolismesDto = this.metabolismeService.initialiserMaintientSecheMasse(utilisateurDto, rythmeDto);
    	this.associerMetabolismes(utilisateurDto, metabolismesDto);
    	
    	// actualisation de l'utilisateur
        return this.utilisateurService.enregistrerUtilisateur(utilisateurDto);
    }
    
    /**
     * Mettre à jour le gabarit.
     * Mettre à jour les métabolismes
     *
     * @param utilisateurDto the utilisateur dto
     * @param gabaritDto the gabarit dto
     * @return the utilisateur DTO
     */
    public UtilisateurDto actualiserGabarit(UtilisateurDto utilisateurDto, GabaritDto gabaritDto) {
    	
    	// actualisation du gabarit
    	gabaritDto = this.gabaritService.enregistrerGabarit(gabaritDto);
    	this.associerGabarit(utilisateurDto, gabaritDto);
    	
    	// actualisation des métabolismes
    	RythmeDto rythmeDto = this.rythmeService.rechercherRythmeParUtilisateur(utilisateurDto);
    	List<MetabolismeDto> metabolismesDto = this.metabolismeService.actualiserTout(utilisateurDto, gabaritDto, rythmeDto);
    	this.associerMetabolismes(utilisateurDto, metabolismesDto);
    	
    	// actualisation de l'utilisateur
    	return this.utilisateurService.enregistrerUtilisateur(utilisateurDto);
    }
    
    /**
     * Actualiser rythme.
     *
     * @param utilisateurDto the utilisateur dto
     * @param rythmeDto the rythme dto
     * @return the utilisateur DTO
     */
    public UtilisateurDto actualiserRythme(UtilisateurDto utilisateurDto, RythmeDto rythmeDto) {
    
    	// actualisation du rythme
    	rythmeDto = this.rythmeService.enregistrerRythme(rythmeDto);
        this.associerRythme(utilisateurDto, rythmeDto);
        
        // actualisation des métabolismes
        GabaritDto gabaritDto = this.gabaritService.rechercherParUtilisateur(utilisateurDto);
        List<MetabolismeDto> metabolismesDto = this.metabolismeService.actualiserTout(utilisateurDto, gabaritDto, rythmeDto);
        this.associerMetabolismes(utilisateurDto, metabolismesDto);
        
        // actualisation de l'utilisateur
        return this.utilisateurService.enregistrerUtilisateur(utilisateurDto);
    }

	
}
