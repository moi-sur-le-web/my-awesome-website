package fr.nico.myawesomewebsite.domaine.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "TAG")
public class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "LIBELLE")
    private String libelle;

    @Column(name = "NB_ATTRIBUTIONS")
    private long nbAttributions;

    @ManyToMany(mappedBy = "tags")
    private List<Fil> fils = new ArrayList<>();

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return this.libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public long getNbAttributions() {
        return this.nbAttributions;
    }

    public void setNbAttributions(long nbAttributions) {
        this.nbAttributions = nbAttributions;
    }

    public List<Fil> getFils() {
        return this.fils;
    }

    public void setFils(List<Fil> fils) {
        this.fils = fils;
    }
}
