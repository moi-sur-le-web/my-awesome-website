package fr.nico.myawesomewebsite.domaine.entities;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import fr.nico.myawesomewebsite.domaine.referentiel.ProfilEnum;
import fr.nico.myawesomewebsite.domaine.referentiel.SexeEnum;

/**
 * The Class UtilisateurEntity.
 */
@Entity
@Table(name = "UTILISATEUR")
public class Utilisateur {

    /** The id. */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    /** The nom. */
    @Column(name = "NOM")
    private String nom;
    
    /** The prenom. */
    @Column(name = "PRENOM")
    private String prenom;
    
    /** The sexe. */
    private SexeEnum sexe;
    
    /** The annee de naissance. */
    @Column(name = "ANNEE_NAISSANCE")
    private int anneeDeNaissance;
    
    /** The mail. */
    @Column(name = "ADRESSE_MAIL")
    private String mail;
    
    /** The mot de passe. */
    @Column(name = "MOT_DE_PASSE")
    private String motDePasse;    
    
    /** The profil. */
    @Column(name = "PROFIL")
    private ProfilEnum profil;
    
    /** The gabarit. */
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GAB_ID", foreignKey = @ForeignKey(name = "FK_UTI_GAB"))
    private Gabarit gabarit;
    
    /** The metabolismes. */
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "UTI_ID", foreignKey = @ForeignKey(name = "FK_MET_UTI"))
    private Set<Metabolisme> metabolismes = new LinkedHashSet<>();

    /** The rythme d'entrainement hebdomadaire et d'alimentation journalier. */
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RTM_ID", foreignKey = @ForeignKey(name = "FK_UTI_RTM"))
    private Rythme rythme;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the nom.
	 *
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Sets the nom.
	 *
	 * @param nom the new nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Gets the prenom.
	 *
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * Sets the prenom.
	 *
	 * @param prenom the new prenom
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * Gets the sexe.
	 *
	 * @return the sexe
	 */
	public SexeEnum getSexe() {
		return sexe;
	}

	/**
	 * Sets the sexe.
	 *
	 * @param sexe the new sexe
	 */
	public void setSexe(SexeEnum sexe) {
		this.sexe = sexe;
	}

	/**
	 * Gets the annee de naissance.
	 *
	 * @return the annee de naissance
	 */
	public int getAnneeDeNaissance() {
		return anneeDeNaissance;
	}

	/**
	 * Sets the annee de naissance.
	 *
	 * @param anneeDeNaissance the new annee de naissance
	 */
	public void setAnneeDeNaissance(int anneeDeNaissance) {
		this.anneeDeNaissance = anneeDeNaissance;
	}

	/**
	 * Gets the mail.
	 *
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * Sets the mail.
	 *
	 * @param mail the new mail
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * Gets the mot de passe.
	 *
	 * @return the mot de passe
	 */
	public String getMotDePasse() {
		return motDePasse;
	}

	/**
	 * Sets the mot de passe.
	 *
	 * @param motDePasse the new mot de passe
	 */
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	/**
	 * Gets the profil.
	 *
	 * @return the profil
	 */
	public ProfilEnum getProfil() {
		return profil;
	}

	/**
	 * Sets the profil.
	 *
	 * @param profil the new profil
	 */
	public void setProfil(ProfilEnum profil) {
		this.profil = profil;
	}

	/**
	 * Gets the gabarit.
	 *
	 * @return the gabarit
	 */
	public Gabarit getGabarit() {
		return gabarit;
	}

	/**
	 * Sets the gabarit.
	 *
	 * @param gabarit the new gabarit
	 */
	public void setGabarit(Gabarit gabarit) {
		this.gabarit = gabarit;
	}

	/**
	 * Gets the metabolismes.
	 *
	 * @return the metabolismes
	 */
	public Set<Metabolisme> getMetabolismes() {
		return metabolismes;
	}

	/**
	 * Sets the metabolismes.
	 *
	 * @param metabolismes the new metabolismes
	 */
	public void setMetabolismes(Set<Metabolisme> metabolismes) {
		this.metabolismes = metabolismes;
	}

	/**
	 * Gets the rythme.
	 *
	 * @return the rythme
	 */
	public Rythme getRythme() {
		return rythme;
	}

	/**
	 * Sets the rythme.
	 *
	 * @param rythme the new rythme
	 */
	public void setRythme(Rythme rythme) {
		this.rythme = rythme;
	}

}
