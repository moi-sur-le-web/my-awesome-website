package fr.nico.myawesomewebsite.domaine.entities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "FIL")
@SQLDelete(sql = "UPDATE FIL SET SUPPRIME = true WHERE id=?")
@Where(clause = "SUPPRIME=false")
public class Fil {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "DATE_CREATION")
    private LocalDateTime dateCreation;

    @Column(name = "DATE_MISE_A_JOUR")
    private LocalDateTime dateMiseAJour;

    @Column(name = "TITRE")
    private String titre;

    @Column(name = "MESSAGE", columnDefinition = "TEXT")
    private String message;

    @Column(name = "HTML_MESSAGE", columnDefinition = "TEXT")
    private String htmlMessage;

    @Column(name = "LIEN")
    private String lien;

    @Column(name = "NOM_IMAGE")
    private String nomImage;

    @JoinColumn(name = "UTI_ID", foreignKey = @ForeignKey(name = "FK_FIL_UTI"))
    @ManyToOne(fetch = FetchType.LAZY)
    private Utilisateur auteur;

    @Column(name = "SUPPRIME")
    private boolean deleted = Boolean.FALSE;

    @Column(name = "APPERCU")
    private boolean appercu = Boolean.TRUE;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "FIL_TAG",
            joinColumns = @JoinColumn(name = "FIL_ID"),
            inverseJoinColumns = @JoinColumn(name = "TAG_ID"),
            uniqueConstraints = @UniqueConstraint(columnNames = { "FIL_ID", "TAG_ID" }, name = "PK_FIL_ID_TAG_ID"))
    private List<Tag> tags = new ArrayList<>();

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(LocalDateTime dateCreation) {
        this.dateCreation = dateCreation;
    }

    public LocalDateTime getDateMiseAJour() {
        return dateMiseAJour;
    }

    public void setDateMiseAJour(LocalDateTime dateMiseAJour) {
        this.dateMiseAJour = dateMiseAJour;
    }

    public String getTitre() {
        return this.titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getHtmlMessage() {
        return this.htmlMessage;
    }

    public void setHtmlMessage(String htmlMessage) {
        this.htmlMessage = htmlMessage;
    }

    public String getLien() {
        return this.lien;
    }

    public void setLien(String lien) {
        this.lien = lien;
    }

    public String getNomImage() {
        return this.nomImage;
    }

    public void setNomImage(String nomImage) {
        this.nomImage = nomImage;
    }

    public Utilisateur getAuteur() {
        return this.auteur;
    }

    public void setAuteur(Utilisateur auteur) {
        this.auteur = auteur;
    }

    public boolean isDeleted() {
        return this.deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isAppercu() {
        return this.appercu;
    }

    public void setAppercu(boolean appercu) {
        this.appercu = appercu;
    }

    public List<Tag> getTags() {
        return this.tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }
}