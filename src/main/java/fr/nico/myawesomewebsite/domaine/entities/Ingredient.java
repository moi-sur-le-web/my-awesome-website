package fr.nico.myawesomewebsite.domaine.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class Ingredient.
 */
@Entity
@Table(name = "INGREDIENT")
public class Ingredient {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;
	
	/** The nom. */
	@Column(name = "NOM")
	private String nom;
	
	/** The code barre. */
	@Column(name = "CODE_BARRE")
	private String codeBarre;
	
	/** The proteines. */
	@Column(name = "PROTEINES")
	private double proteines;
	
	/** The glucides. */
	@Column(name = "GLUCIDES")
	private double glucides;
	
	/** The lipides. */
	@Column(name = "LIPIDES")
	private double lipides;
	
	/** The energie. */
	@Column(name = "ENERGIE")
	private int energie;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the nom.
	 *
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Sets the nom.
	 *
	 * @param nom the new nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Gets the code barre.
	 *
	 * @return the code barre
	 */
	public String getCodeBarre() {
		return codeBarre;
	}

	/**
	 * Sets the code barre.
	 *
	 * @param codeBarre the new code barre
	 */
	public void setCodeBarre(String codeBarre) {
		this.codeBarre = codeBarre;
	}

	/**
	 * Gets the proteines.
	 *
	 * @return the proteines
	 */
	public double getProteines() {
		return proteines;
	}

	/**
	 * Sets the proteines.
	 *
	 * @param proteines the new proteines
	 */
	public void setProteines(double proteines) {
		this.proteines = proteines;
	}

	/**
	 * Gets the glucides.
	 *
	 * @return the glucides
	 */
	public double getGlucides() {
		return glucides;
	}

	/**
	 * Sets the glucides.
	 *
	 * @param glucides the new glucides
	 */
	public void setGlucides(double glucides) {
		this.glucides = glucides;
	}

	/**
	 * Gets the lipides.
	 *
	 * @return the lipides
	 */
	public double getLipides() {
		return lipides;
	}

	/**
	 * Sets the lipides.
	 *
	 * @param lipides the new lipides
	 */
	public void setLipides(double lipides) {
		this.lipides = lipides;
	}

	/**
	 * Gets the energie.
	 *
	 * @return the energie
	 */
	public int getEnergie() {
		return energie;
	}

	/**
	 * Sets the energie.
	 *
	 * @param energie the new energie
	 */
	public void setEnergie(int energie) {
		this.energie = energie;
	}
}
