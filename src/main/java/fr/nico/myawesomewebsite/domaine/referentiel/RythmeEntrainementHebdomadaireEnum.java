package fr.nico.myawesomewebsite.domaine.referentiel;

/**
 * The Enum RythmeEntrainementHebdomadaireEnum.
 */
public enum RythmeEntrainementHebdomadaireEnum {

	/** The zero. */
	ZERO("Aucun", 1.200),
	
	/** The un. */
	UN("Faible", 1.375),
	
	/** The deux. */
	DEUX("Modéré", 1.550),
	
	/** The trois. */
	TROIS("Elevé", 1.725),
	
	/** The quatre. */
	QUATRE("Très élevé", 1.900);
	
	/** The libellé. */
	private String libelle;
	
	/** The coefficient. */
	private double coefficient;
	
	/**
	 * Instantiates a new rythme entrainement hebdomadaire enum.
	 *
	 * @param libellé the libellé
	 * @param coefficient the coefficient
	 */
	private RythmeEntrainementHebdomadaireEnum(String libelle, double coefficient) {
		
		this.libelle = libelle;
		this.coefficient = coefficient;
	}

	/**
	 * Gets the libellé.
	 *
	 * @return the libellé
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * Gets the coefficient.
	 *
	 * @return the coefficient
	 */
	public double getCoefficient() {
		return coefficient;
	}
	
}
