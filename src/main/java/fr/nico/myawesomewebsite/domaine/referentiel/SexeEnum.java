package fr.nico.myawesomewebsite.domaine.referentiel;

public enum SexeEnum {

    H("Homme", "Masculin"),
    F("Femme", "Féminin"),
    A("Autre", "Autre");

    private final String sexe;

    private final String genre;

    private SexeEnum(String sexe, String genre) {
        this.sexe = sexe;
        this.genre = genre;
    }

    public String getSexe() {
        return this.sexe;
    }

    public String getGenre() {
        return this.genre;
    }

}
