package fr.nico.myawesomewebsite.domaine.referentiel;

/**
 * The Enum MetabolismeTypeEnum.
 */
public enum TypeMetabolismeEnum {

    /** The basal. */
    BASAL("Basal", "basal"),

    /** The maintient. */
    MAINTIENT("De maintient", "maintient"),

    /** The seche. */
    SECHE("En sèche", "sèche"),

    /** The masse. */
    MASSE("De prise de masse", "masse");

    /** The nom. */
    private String nom;

    /** The nom court. */
    private String nomCourt;

    /**
     * Instantiates a new type metabolisme enum.
     *
     * @param nom the nom
     * @param nomCourt the nom court
     */
    TypeMetabolismeEnum(String nom, String nomCourt) {

        this.nom = nom;
        this.nomCourt = nomCourt;
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Gets the nom court.
     *
     * @return the nom court
     */
    public String getNomCourt() {
        return this.nomCourt;
    }

}
