package fr.nico.myawesomewebsite.domaine.referentiel;

/**
 * The Enum SortDirectionEnum.
 */
public enum SortDirectionEnum {

    // @formatter:off
    ASC("asc"),
    DESC("desc");
    // @formatter:on

    private String displayValue;

    private SortDirectionEnum(String value) {
        this.displayValue = value;
    }

    public String getDisplayValue() {
        return this.displayValue;
    }

}
