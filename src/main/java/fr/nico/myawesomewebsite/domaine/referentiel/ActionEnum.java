package fr.nico.myawesomewebsite.domaine.referentiel;

public enum ActionEnum {

    SUPPRIMER("supprimé"), 
    ENREGISTRER("enregistré"), 
    MIS_A_JOUR("mis à jour");

    private final String displayValue;

    private ActionEnum(String action) {
        this.displayValue = action;
    }

    public String getDisplayValue() {
        return this.displayValue;
    }

    @Override
    public String toString() {
        return this.displayValue;
    }
}
