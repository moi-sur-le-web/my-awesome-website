package fr.nico.myawesomewebsite.domaine.exception;

/**
 * The Class TechniqueException.
 */
public class TechniqueException extends Exception {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 9120687537350262758L;

    /**
     * Instantiates a new technique exception.
     *
     * @param message the message
     */
    public TechniqueException(String message) {
        super(message);
    }
}
