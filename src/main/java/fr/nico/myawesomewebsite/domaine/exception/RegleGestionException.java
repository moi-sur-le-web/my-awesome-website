package fr.nico.myawesomewebsite.domaine.exception;

/**
 * The Class MyException.
 */
public class RegleGestionException extends RuntimeException
{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4480029612519467333L;

    /**
     * Instantiates a new my exception.
     *
     * @param message the message
     */
    public RegleGestionException(String message) {
        super(message);
    }

}
