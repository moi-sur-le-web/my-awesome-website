package fr.nico.myawesomewebsite.domaine.exception;

/**
 * The Class FilServiceException.
 */
public class FilServiceException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6284499832392268633L;

	/**
	 * Instantiates a new fil service exception.
	 *
	 * @param message the message
	 */
	public FilServiceException(String message) {
		super(message);
	}
}
