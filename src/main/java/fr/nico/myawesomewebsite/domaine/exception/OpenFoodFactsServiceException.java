package fr.nico.myawesomewebsite.domaine.exception;

/**
 * The Class OpenFoodFactsServiceException.
 */
public class OpenFoodFactsServiceException extends Exception {
	
	/**
	 * The Enum OpenFoodFactsServiceExceptionEnum.
	 */
	public enum OpenFoodFactsServiceExceptionEnum {
		
		/** The produit inconnu. */
		PRODUIT_INCONNU(100, "Produit inconnu"),
		
		/** The format code barre invalide. */
		FORMAT_CODE_BARRE_INVALIDE(200, "Code barre invalide"),
		
		/** The erreur serveur. */
		ERREUR_SERVEUR(300, "Erreur OpenFoodFacts"),
		
		/** The item existant. */
		ITEM_EXISTANT(400, "Item déjà présent en base de données");
		
		/** The code. */
		private int code;
		
		/** The message. */
		private String message;
		
		/**
		 * Instantiates a new open food facts service exception enum.
		 *
		 * @param code the code
		 * @param message the message
		 */
		private OpenFoodFactsServiceExceptionEnum(int code, String message) {
			
			this.code = code;
			this.message = message;
		}

		/**
		 * Gets the code.
		 *
		 * @return the code
		 */
		public int getCode() {
			return code;
		}

		/**
		 * Gets the message.
		 *
		 * @return the message
		 */
		public String getMessage() {
			return message;
		}
	}

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1569199893678289427L;
	
	/** The type. */
	private OpenFoodFactsServiceExceptionEnum type;

	/**
	 * Instantiates a new open food facts exception.
	 *
	 * @param type the produit inconnu
	 */
	public OpenFoodFactsServiceException(OpenFoodFactsServiceExceptionEnum type) {
		 
		super(type.getMessage());
		this.type = type;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public OpenFoodFactsServiceExceptionEnum getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(OpenFoodFactsServiceExceptionEnum type) {
		this.type = type;
	}

}
