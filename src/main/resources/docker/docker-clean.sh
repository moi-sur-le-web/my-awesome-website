#!/bin/bash

# suppression des conteneurs
docker container rm my_java_app_container
docker container rm my_postgres_container

# suppression des images
docker image rm my_maven_image:latest
docker image rm my_java_app_image:latest
docker image rm my_postgres_image:latest

# purges
docker image prune -f
docker container prune -f

if [ -e ./java/myawesomewebapp.jar ] 
then
    rm ./java/myawesomewebapp.jar
fi
