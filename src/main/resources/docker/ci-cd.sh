#!/bin/bash

readonly HOST_USER=$(whoami)
readonly HOST_SOURCES_PROJECT="/home/${HOST_USER}/git/my-awesome-website"
readonly CONTAINER_SOURCES_PROJECT="/var/tmp/build"
readonly HOST_DOCKER_FOLDER=${HOST_SOURCES_PROJECT}/src/main/resources/docker
readonly HOST_M2="/home/${HOST_USER}/.m2"
readonly CONTAINER_M2="/var/tmp/.m2"
readonly MAVEN_IMAGE_NAME="my_maven_image"
readonly MAVEN_CONTAINER_NAME="my_maven_container"

crlf() {
	echo ""
}

# BUILD
crlf
echo "[INFO] Construction de l'image Maven : ${MAVEN_IMAGE_NAME}."
crlf
docker image build -t ${MAVEN_IMAGE_NAME} ./maven

crlf
echo "[INFO] Lancement du build Maven : ${MAVEN_CONTAINER_NAME}."
crlf
docker run -it --rm --name ${MAVEN_CONTAINER_NAME} \
	-v ${HOST_SOURCES_PROJECT}:${CONTAINER_SOURCES_PROJECT} \
	-v ${HOST_M2}:${CONTAINER_M2} \
	-w ${CONTAINER_SOURCES_PROJECT} \
	${MAVEN_IMAGE_NAME}

crlf
echo "[INFO] Copie du jar produit de ${HOST_SOURCES_PROJECT} vers ${HOST_DOCKER_FOLDER}."
cp ${HOST_SOURCES_PROJECT}/target/*.jar ${HOST_DOCKER_FOLDER}/java/myawesomewebapp.jar

# DEPLOY
crlf
echo "[INFO] Lancement du Docker compose."
crlf
docker compose up &
