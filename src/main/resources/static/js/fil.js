//
// js de suppression d'un fil

function initListeFils() {
	forEachAddEventListener( document.getElementsByName("btnSupprimer"), "click", setIdFormDelete );
	document.getElementById("suppressionConfirmer").addEventListener( "click" , submitDeleteForm );
}

function setIdFormDelete(event) {
	document.getElementsByName("id").forEach( (el) => {
		el.value = event.target.closest(".suppr").id;
	});
}

function submitDeleteForm(event) {
	event.target.closest("form").submit();
}

//
// actions édition fil

function initEditerFil() {
	document.getElementById("supprimerImageActuelle").addEventListener("change", setFileInputState);
}

function setFileInputState(e) {
	let inputImage = document.getElementById("nouvelleImage");
	if (e.target.checked) {
		inputImage.setAttribute("readonly", "readonly");
		inputImage.value = "";
	} else {
		inputImage.removeAttribute("readonly");
	}
}

//
// actions au chargement

window.addEventListener( "load" , initListeFils );
window.addEventListener( "load" , initEditerFil );