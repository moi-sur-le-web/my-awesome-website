function initListeFils() {
	forEachInArrayAddEventListener( document.getElementsByTagName("td"), "click", triggerLink );

}

function triggerLink(event) {
	let annuaire = ( document.getElementById("description").textContent == 'L\'annuaire' );
	if (annuaire){
		window.open(event.target.closest("tr").getAttribute("data-lien"), '_blank');
	} else {
		window.location = event.target.closest("tr").getAttribute("data-lien");
	}
}

window.addEventListener( "load" , initListeFils );