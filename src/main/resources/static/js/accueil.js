function init() {
	document.getElementById("voirMdp").addEventListener( "click" , voirOuCacherMotDePasse );
}

function voirOuCacherMotDePasse(e) {
	let eyeBtn = e.target;
	let passwdInput = document.getElementById("motDePasse");
	if (passwdInput.getAttribute("type") == "password") {
		passwdInput.setAttribute("type", "text");
		eyeBtn.classList.remove("bi-eye-fill");
		eyeBtn.classList.add("bi-eye-slash-fill");
	} else {
		passwdInput.setAttribute("type", "password");
		eyeBtn.classList.add("bi-eye-fill");
		eyeBtn.classList.remove("bi-eye-slash-fill");
	}
}

window.addEventListener( "load" , init );