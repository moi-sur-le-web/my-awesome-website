package fr.nico.myawesomewebsite.unitaire;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.junit.jupiter.api.Test;

class TestMisc {

    @Test
    void test_timeZone() {
        Calendar calendar = Calendar.getInstance();
        assertEquals(calendar.getTimeZone(), TimeZone.getTimeZone("Europe/Paris"));
    }
    
    //@Test
    void test_new_date() {
        String today = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        assertEquals("2022-12-28",today);
    }
    
    @Test
    void test_new_date_and_time() {
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);
        System.out.println(now.toLocalDate());
        
        LocalDateTime yesterday = LocalDateTime.of(2022,9,7,23,59,59,99);
        System.out.println(yesterday);
        
        assertTrue(now.isAfter(yesterday));
    }

}
