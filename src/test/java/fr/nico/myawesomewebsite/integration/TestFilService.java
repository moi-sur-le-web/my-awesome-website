package fr.nico.myawesomewebsite.integration;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import fr.nico.myawesomewebsite.domaine.entities.Fil;
import fr.nico.myawesomewebsite.domaine.entities.Tag;
import fr.nico.myawesomewebsite.domaine.entities.Utilisateur;
import fr.nico.myawesomewebsite.domaine.exception.FilServiceException;
import fr.nico.myawesomewebsite.metier.BlogService;
import fr.nico.myawesomewebsite.persistance.FilRepository;
import fr.nico.myawesomewebsite.persistance.TagRepository;
import fr.nico.myawesomewebsite.persistance.UtilisateurRepository;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilResumeDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilToSaveDto;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@ActiveProfiles(profiles = { "test" })
class TestFilService {

    private static final String HTTP_WWW_GOOGLE_FR = "http://www.google.fr";

    private static final String MESSAGE_FIL = "message_fil";

    private static final String TITRE_FIL = "titre_fil";

    private static final String NOM_AUTEUR = "nom_auteur";

    private static final String PRENOM_AUTEUR = "prenom_auteur";

    @Autowired
    private FilRepository filRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private BlogService filService;

    private Utilisateur auteur;

    @BeforeEach
    void init() {

        this.auteur = new Utilisateur();
        this.auteur.setPrenom(PRENOM_AUTEUR);
        this.auteur.setNom(NOM_AUTEUR);
        this.auteur = this.utilisateurRepository.save(this.auteur);
    }

    @AfterEach
    void cleanDb() {
        this.filRepository.deleteAll();
        this.utilisateurRepository.deleteAll();
    }

    //@Test
    @DisplayName("Test d'enregistrement d'un fil minimaliste")
    void enregistrerFilMinimal() throws FilServiceException {

        FilToSaveDto filToSaveDto = new FilToSaveDto();
        filToSaveDto.setTitre(TITRE_FIL);
        filToSaveDto.setMessage(MESSAGE_FIL);
        FilResumeDto filresumeDto = this.filService.enregistrerFil(this.auteur, filToSaveDto);
        Assertions.assertSame(TITRE_FIL, filresumeDto.getTitre());
    }

    //@Test
    @DisplayName("Test d'enregistrement d'un fil avec auteur null")
    void enregistrerFilAuteurNull() throws FilServiceException {

        Assertions.assertThrows(FilServiceException.class, () -> {
            this.filService.enregistrerFil(null, new FilToSaveDto());
        });
    }

    //@Test
    @DisplayName("Test d'enregistrement d'un fil avec un lien")
    void enregistrerFilAvecLien() throws FilServiceException {

        FilToSaveDto filToSaveDto = new FilToSaveDto();
        filToSaveDto.setTitre(TITRE_FIL);
        filToSaveDto.setMessage(MESSAGE_FIL);
        filToSaveDto.setLien(HTTP_WWW_GOOGLE_FR);
        FilResumeDto filResumeDto = this.filService.enregistrerFil(this.auteur, filToSaveDto);

        Optional<Fil> fil = this.filRepository.findById(filResumeDto.getId());
        Assertions.assertEquals(HTTP_WWW_GOOGLE_FR, fil.get().getLien());
    }

    //@Test
    @DisplayName("Test d'enregistrement d'un fil avec des tags")
    void enregistrerFilAvecTags() throws FilServiceException {

        FilToSaveDto filToSaveDto = new FilToSaveDto();
        filToSaveDto.setTitre(TITRE_FIL);
        filToSaveDto.setMessage(MESSAGE_FIL);
        filToSaveDto.setTags("tag1,tag2,tag3");
        this.filService.enregistrerFil(this.auteur, filToSaveDto);

        List<Tag> tags = this.tagRepository.findAll();
        Assertions.assertEquals(3, tags.size());
    }

    //@Test
    @DisplayName("Test d'enregistrement de 2 fils avec des tags en communs")
    void enregistrerFilsAvecTagsEnCommun() throws FilServiceException {

        FilToSaveDto filToSaveDto1 = new FilToSaveDto();
        filToSaveDto1.setTitre(TITRE_FIL);
        filToSaveDto1.setMessage(MESSAGE_FIL);
        filToSaveDto1.setTags("tag1,tag2,tag3");
        this.filService.enregistrerFil(this.auteur, filToSaveDto1);

        FilToSaveDto filToSaveDto2 = new FilToSaveDto();
        filToSaveDto2.setTitre(TITRE_FIL);
        filToSaveDto2.setMessage(MESSAGE_FIL);
        filToSaveDto2.setTags("tag2,tag3,tag4,tag5");
        this.filService.enregistrerFil(this.auteur, filToSaveDto2);

        List<Tag> tags = this.tagRepository.findAll();
        Assertions.assertEquals(5, tags.size());
    }

    //@Test
    @DisplayName("Tests HTML")
    void enregistrerFilsTestHtml() throws FilServiceException {

        FilToSaveDto filToSaveDto = new FilToSaveDto();
        filToSaveDto.setTitre(TITRE_FIL);
        filToSaveDto.setMessage(MESSAGE_FIL);
        FilResumeDto filResumeDto = this.filService.enregistrerFil(this.auteur, filToSaveDto);
        Optional<Fil> fil = this.filRepository.findById(filResumeDto.getId());
        Assertions.assertEquals("<p>message_fil</p>\n", fil.get().getHtmlMessage());

        filToSaveDto.setMessage("<SCRIPT SRC=http://xss.rocks/xss.js></SCRIPT>");
        filResumeDto = this.filService.enregistrerFil(this.auteur, filToSaveDto);
        fil = this.filRepository.findById(filResumeDto.getId());
        Assertions.assertEquals("<SCRIPT SRC=http://xss.rocks/xss.js></SCRIPT>\n", fil.get().getHtmlMessage());

    }
}